#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClient6.h>
#include "lwip/dns.h"
void waitIPv6()
{
  while (WiFi.localIPv6()[0] == 0)
  {
    delay(100);
    Serial.print(".");
  }
  Serial.print("Local IPv6: ");
  Serial.println(WiFi.localIPv6());
}
void setup()
{
  const char *ssid = "1111";
  const char *password = "1111";
  const char *host = "6.ipw.cn";
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }
  WiFi.enableIpV6();
  Serial.print("Local IPv4: ");
  Serial.println(WiFi.localIP());
  Serial.println(" CONNECTED");
  Serial.println("---------------");
  waitIPv6();

  Serial.println("Trying client6");
  WiFiClient6 client1;
  client1.connect(host, 20054, 3000);
  if (client1.connected())
  {
    Serial.println("Connected 6!");
  }
  Serial.println("Finished!");
}

void loop()
{
  delay(100);
}