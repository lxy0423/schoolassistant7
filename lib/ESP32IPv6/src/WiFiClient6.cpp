#include <WiFiClient6.h>
#include "lwip/dns.h"

int WiFiClient6::connect(IPv6Address ip, uint16_t port, int32_t timeout)
{
    _timeout = timeout;
    int sockfd = socket(AF_INET6, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        log_e("socket: %d", errno);
        return 0;
    }
    fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL, 0) | O_NONBLOCK);

    struct sockaddr_in6 serveraddr;
    // const uint8_t *addr = ip;
    memset((char *)&serveraddr, 0, sizeof(serveraddr));
    // memcpy((void *)&serveraddr.sin6_addr.s6_addr, (const void *)(&addr), 16);
    serveraddr.sin6_family = AF_INET6;
    if (inet_pton(AF_INET6, ip.toString().c_str(), &serveraddr.sin6_addr) < 0)
    {
        log_e("[WIFIClient6] inet_pton error");
        return 0;
    }
    serveraddr.sin6_port = htons(port);
    fd_set fdset;
    struct timeval tv;
    FD_ZERO(&fdset);
    FD_SET(sockfd, &fdset);
    tv.tv_sec = _timeout / 1000;
    tv.tv_usec = 0;

#ifdef ESP_IDF_VERSION_MAJOR
    int res = lwip_connect(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
#else
    int res = lwip_connect_r(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
#endif
    if (res < 0 && errno != EINPROGRESS)
    {
        log_e("connect on fd %d, errno: %d, \"%s\"", sockfd, errno, strerror(errno));
        close(sockfd);
        return 0;
    }

    res = select(sockfd + 1, nullptr, &fdset, nullptr, _timeout < 0 ? nullptr : &tv);
    if (res < 0)
    {
        log_e("select on fd %d, errno: %d, \"%s\"", sockfd, errno, strerror(errno));
        close(sockfd);
        return 0;
    }
    else if (res == 0)
    {
        log_i("select returned due to timeout %d ms for fd %d", _timeout, sockfd);
        close(sockfd);
        return 0;
    }
    else
    {
        int sockerr;
        socklen_t len = (socklen_t)sizeof(int);
        res = getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &sockerr, &len);

        if (res < 0)
        {
            log_e("getsockopt on fd %d, errno: %d, \"%s\"", sockfd, errno, strerror(errno));
            close(sockfd);
            return 0;
        }

        if (sockerr != 0)
        {
            log_e("socket error on fd %d, errno: %d, \"%s\"", sockfd, sockerr, strerror(sockerr));
            close(sockfd);
            return 0;
        }
    }

#define ROE_WIFICLIENT(x, msg)                                                                                 \
    {                                                                                                          \
        if (((x) < 0))                                                                                         \
        {                                                                                                      \
            log_e("Setsockopt '" msg "'' on fd %d failed. errno: %d, \"%s\"", sockfd, errno, strerror(errno)); \
            return 0;                                                                                          \
        }                                                                                                      \
    }
    ROE_WIFICLIENT(setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)), "SO_SNDTIMEO");
    ROE_WIFICLIENT(setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)), "SO_RCVTIMEO");

    // These are also set in WiFiClientSecure, should be set here too?
    // ROE_WIFICLIENT(setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &enable, sizeof(enable)),"TCP_NODELAY");
    // ROE_WIFICLIENT (setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(enable)),"SO_KEEPALIVE");

    fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL, 0) & (~O_NONBLOCK));
    clientSocketHandle.reset(new WiFiClientSocketHandle(sockfd));
    _rxBuffer.reset(new WiFiClientRxBuffer(sockfd));

    _connected = true;
    return 1;
}
int WiFiClient6::connect(const char *host, uint16_t port)
{
    return connect(host, port, _timeout);
}

SemaphoreHandle_t _dns_binary = NULL;
SemaphoreHandle_t _dns_mutex = NULL; // DNS查询锁，防止多线程出问题
volatile bool _dns_has_result = false;
static void _dnscb(const char *name, const ip_addr_t *ipaddr, void *callback_arg)
{
    if (ipaddr)
    {
        (*reinterpret_cast<IPv6Address *>(callback_arg)) = ipaddr->u_addr.ip6.addr;
        _dns_has_result = true;
    }
    xSemaphoreGive(_dns_binary);
}

bool hostByName6(const char *host, IPv6Address &ip)
{
    _dns_has_result = false;
    ip_addr_t ipaddr;
    memset(&ipaddr, 0, sizeof(ip_addr_t));
    xSemaphoreTake(_dns_mutex, portMAX_DELAY);
    xSemaphoreTake(_dns_binary, 0);
    err_t err = dns_gethostbyname(host, &ipaddr, _dnscb, &ip);
    if (err == ERR_OK)
    {
        ip = ipaddr.u_addr.ip6.addr;
        _dns_has_result = true;
    }
    else if (err == ERR_INPROGRESS)
    {
        xSemaphoreTake(_dns_binary, 3000);
    }
    xSemaphoreGive(_dns_mutex);
    return _dns_has_result;
}

int WiFiClient6::connect(const char *host, uint16_t port, int32_t timeout)
{
    if (_dns_binary == NULL)
    {
        _dns_binary = xSemaphoreCreateBinary();
        xSemaphoreTake(_dns_binary, 0);
    }
    if (_dns_mutex == NULL)
    {
        _dns_mutex = xSemaphoreCreateMutex();
    }
    IPv6Address srv;
    if (hostByName6(host, srv) == false)
    {
        return 0;
    }
    return connect(srv, port, timeout);
}