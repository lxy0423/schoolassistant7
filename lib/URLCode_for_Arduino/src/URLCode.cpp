
#include "URLCode.h"

int URLCode :: hex2dec(char c){
    if ('0' <= c && c <= '9') 
    {
        return c - '0';
    } 
    else if ('a' <= c && c <= 'f')
    {
        return c - 'a' + 10;
    } 
    else if ('A' <= c && c <= 'F')
    {
        return c - 'A' + 10;
    } 
    else 
    {
        return -1;
    }
}


char URLCode :: dec2hex(short int c)
{
    if (0 <= c && c <= 9) 
    {
        return c + '0';
    } 
    else if (10 <= c && c <= 15) 
    {
        return c + 'A' - 10;
    } 
    else 
    {
        return -1;
    }
}


// Chinese 编码一个url
// English Encode URLCode
String URLCode :: urlencode(String str)
{
    String urlcode = "";
    int i = 0;
    int len = str.length();
    for (i = 0; i < len; ++i) 
    {
        char c = str[i];
        if (    ('0' <= c && c <= '9') ||
                ('a' <= c && c <= 'z') ||
                ('A' <= c && c <= 'Z') || 
                c == '/' || c == '.') 
        {
            urlcode += String(c);
        } 
        else 
        {
            int j = (short int)c;
            if (j < 0)
                j += 256;
            int i1, i0;
            i1 = j / 16;
            i0 = j - i1 * 16;
            urlcode += String('%');
            urlcode += String((char)dec2hex(i1));
            urlcode += String((char)dec2hex(i0));
        }
    }
    return urlcode;
}

// Chinese 解码url
// English Decode URLCode
String URLCode :: urldecode(String str)
{
    String strcode = "";
    int i = 0;
    int len = str.length();
    for (i = 0; i < len; ++i) 
    {
        char c = str[i];
        if (c != '%') {
            strcode += String(c);
        } else {
            char c1 = str[++i];
            char c0 = str[++i];
            int num = 0;
            num = hex2dec(c1) * 16 + hex2dec(c0);
            strcode += String((char)num);
        }
    }
    return strcode;
}
