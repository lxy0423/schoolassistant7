#include <ThermalPrinter.h>
ThermalPrinter printer;
void ThermalPrinter::sendReg(uint8_t reg, uint8_t data)
{
    _Wire.beginTransmission(i2c_addr);
    _Wire.write(reg);
    _Wire.write(data);
    _Wire.endTransmission();
}
bool ThermalPrinter::testConnection()
{
    _Wire.beginTransmission(i2c_addr);
    return _Wire.endTransmission() == 0;
}
void ThermalPrinter::go(int steps)
{
    sendReg(0x01, steps);
}

void ThermalPrinter::gomm(int mm)
{
    sendReg(0x02, mm);
}
void ThermalPrinter::sendLine(uint8_t *lineData)
{
    uint8_t timeH, timeL;
    uint16_t printTime[6] = {_intensity, _intensity, _intensity, _intensity, _intensity, _intensity};
    uint8_t retryCount = 0;
    while (1)
    {
        _Wire.beginTransmission(i2c_addr);
        _Wire.write(0x4c);
        _Wire.endTransmission();
        _Wire.requestFrom(i2c_addr, 1);
        if (_Wire.read() != 'P')
            break;
        delayMicroseconds(2000);
        retryCount++;
        if (retryCount > 100)
        {
            Serial.println("Printer Error.");
            return;
        }
    }
    _Wire.beginTransmission(i2c_addr);
    _Wire.write(0x10);
    _Wire.write(lineData, 48);
    for (uint8_t i = 0; i < 6; ++i)
    {
        timeH = printTime[i] >> 8;
        timeL = printTime[i] & 0xff;
        _Wire.write(timeH);
        _Wire.write(timeL);
    }
    _Wire.write('P');
    _Wire.endTransmission();
}
void ThermalPrinter::endPrint()
{
    sendReg(0x4C, 'X');
}
void ThermalPrinter::setHeatIntensity(uint16_t intensity)
{
    _intensity = intensity;
}
int ThermalPrinter::getThermalADC()
{
    int res;
    _Wire.beginTransmission(i2c_addr);
    _Wire.write(0x04);
    _Wire.endTransmission(false);
    _Wire.requestFrom(i2c_addr, 2, (int)true);

    res = (uint8_t)_Wire.read();
    res <<= 8;
    res |= (uint8_t)_Wire.read();
    return res;
}
int ThermalPrinter::getPaperADC()
{
    int res;
    _Wire.beginTransmission(i2c_addr);
    _Wire.write(0x06);
    _Wire.endTransmission(false);
    _Wire.requestFrom(i2c_addr, 2, (int)true);

    res = (uint8_t)_Wire.read();
    res <<= 8;
    res |= (uint8_t)_Wire.read();
    return res;
}

void ThermalPrinter::drawDot(uint32_t x, uint32_t y, bool isDraw)
{
    if (rotate)
    {
        uint32_t tmp = y;
        y = x;
        x = tmp;
    }
    if (x >= 384)
        return;
    max_y = max(y, max_y);
    uint32_t pos = y * 48 + (x / 8);
    uint8_t bit = x % 8;
    bit = 7 - bit;
    if (isDraw)
        buffer[pos] |= (1 << bit);
    else
        buffer[pos] &= ~(1 << bit);
}
/**
 * @brief 从内存画jpeg
 *
 * @param img 指向图片的指针
 * @param size 文件大小
 * @param option JPEG_SCALE_...
 * @param x 左上角x
 * @param y 左上角y
void ThermalPrinter::drawJPEGFromPtr(const uint8_t *img, size_t size, int option, uint32_t x, uint32_t y, bool use_dither, uint8_t bw_threshold)
{
    if (jpeg.openFLASH((uint8_t *)img, size, JPEGDraw))
    {
        _use_dither = use_dither;
        threshold = bw_threshold;
        jpeg.setPixelType(RGB565_LITTLE_ENDIAN);
        jpeg.decode(x, y, option);
        jpeg.close();
    }
}

void ThermalPrinter::drawJPEGFromFile(File &fp, int option, uint32_t x, uint32_t y, bool use_dither, uint8_t bw_threshold)
{
    if (jpeg.open(fp, JPEGDraw))
    {
        _use_dither = use_dither;
        threshold = bw_threshold;
        jpeg.setPixelType(RGB565_LITTLE_ENDIAN);
        jpeg.decode(x, y, option);
        jpeg.close();
    }
}

 */
void ThermalPrinter::setMaxY(uint32_t new_y)
{
    max_y = new_y;
}

void ThermalPrinter::clearBuffer(uint32_t buffer_size)
{
    memset(buffer, 0, buffer_size);
    max_y = 0;
}

void ThermalPrinter::sendBuffer()
{
    for (uint32_t pointer = max_y + 1; pointer > 0; pointer--)
    {
        printer.sendLine(buffer + pointer * 48 - 48);
    }
    printer.endPrint();
}