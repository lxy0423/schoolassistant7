#ifndef __PAPERANG_H__
#define __PAPERANG_H__
#include <BluetoothSerial.h>
#include "Arduino_CRC32.h"
#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"
#include "esp_spp_api.h"
#include "esp_task_wdt.h"
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

typedef void (*pFuncPrint)(uint8_t *buffer, uint32_t size);
class Paperang
{
private:
    uint8_t gotStartByte = 0;
    uint8_t c;
    uint16_t readpos = 0;
    uint8_t dataPack_read[2048];
    uint16_t dataPack_read_pos = 0;
    Arduino_CRC32 crc32;
    uint8_t dataPack[512];
    uint32_t dataPack_len;
    uint32_t crc32_result;
    BluetoothSerial SerialBT;
    pFuncPrint _startPrint;
    uint8_t *printData = NULL;
    uint32_t printDataCount = 0;
    void send(void);
    void send_ack(uint8_t type);
    void process_data();
    void send_msg(uint8_t type, const uint8_t *dat, uint16_t len);

public:
    void begin();
    void update();
    void setStartPrintFunc(pFuncPrint f);
    void setBuffer(uint8_t *b);
};

extern Paperang paperang;
#endif