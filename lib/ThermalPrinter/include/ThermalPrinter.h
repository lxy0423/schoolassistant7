#ifndef __THERMAL_PRINTER_H__
#define __THERMAL_PRINTER_H__
#include <Arduino.h>
#include <Wire.h>
#include <JPEGDEC.h>
class ThermalPrinter
{
private:
    const uint8_t i2c_addr = 0x11;
    uint16_t _intensity = 4000;
    void sendReg(uint8_t reg, uint8_t data);
    uint32_t max_y = 0;

public:
    /**
     * @brief 测试打印机连接状态
     *
     * @return true 连接成功
     * @return false 打印机未连接
     */
    bool testConnection();
    void go(int steps);
    void gomm(int mm);
    void sendLine(uint8_t *lineData);
    void endPrint();
    void setHeatIntensity(uint16_t intensity = 4000);
    int getThermalADC();
    int getPaperADC();

    void setBuffer(uint8_t *new_buffer)
    {
        buffer = new_buffer;
    }
    void drawDot(uint32_t x, uint32_t y, bool isDraw);
    
    void setMaxY(uint32_t new_y);
    void clearBuffer(uint32_t buffer_size);
    void sendBuffer();
    bool rotate = false;
    uint8_t *buffer = NULL;
    TwoWire &_Wire = Wire1;
};

extern ThermalPrinter printer;
#endif