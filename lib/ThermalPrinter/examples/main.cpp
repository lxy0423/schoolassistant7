#include <Arduino.h>
#include <Paperang.h>
#include <ThermalPrinter.h>
uint8_t *printData;

void startPrint(uint8_t *buffer, uint32_t size)
{
    //Serial.println("[INFO]正在打印...");
    //Serial.printf("[INFO]共%u行\n", size / 48);
    for (uint32_t pointer = 0; pointer < size; pointer += 48)
    {
        printer.sendLine(printData + pointer);
    }
    printer.gomm(5);
    printer.endPrint();
}

void setup()
{
    Serial.begin(115200);
    Wire.setPins(17, 16);
    Wire.begin();
    printData = (uint8_t *)malloc(50 * 1024);
    if (!printData)
    {
        Serial.println("[ERROR]动态内存分配失败!\n");
        while (1)
            ;
    }
    paperang.setBuffer(printData);
    paperang.setStartPrintFunc(startPrint);
    paperang.begin();
}

void loop()
{
    paperang.update();
}