# 校园生活助手 7  
## 已停止开发，release里的二进制文件可以正常点亮，只包含公共API，仅限于测试使用
## 停止开发说明
公开此项目目的是分享一些功能的实现原理，因为仔细看里面很多功能都是首创  
比如Arduino下基于IPv6的WiFiClient（官方到现在只有ESP-IDF支持这个，issue都挂了一年多了）、基于lvgl的Lua解释器、在ESP32上实现mpeg1解码、小米手环4的支持、基于ESPNow的文件传输协议、在ESP32上实现B站扫码登录以及Cookie自动管理、基于彩云天气API的分钟级降雨预报、基于ESP32和lvgl的网易云音乐……  
  
如果在B站，每个功能单独拿出来都能做一个视频  
  
因为是分享代码，所以对可用性不做任何保证，也不保证不会出bug，只是拍视频时可以编译运行，然后照原样上传代码和PCB（后来也加了些新功能）  
现在因为用不到了，而且还要做新视频，所以本项目停止开发。  
并于2023年8月10日起正式停止对此项目的*一切技术支持*  
二进制文件名已经说明烧录方法，如果还是不知道怎么烧录，说明此项目不适合你，建议换个简单的项目复刻  
另外，复刻前，请确认自己能**定位**并解决类似下面这样的报错：  
```
Guru Meditation Error: Core  0 panic'ed (IllegalInstruction). Exception was unhandled.
Memory dump at 0x40103a78: ffffffff ffffffff ffffffff
Core 0 register dump:
PC      : 0x40104003  PS      : 0x00050b30  A0      : 0x8010ebec  A1      : 0x3ffce950  
...(省略)
A14     : 0x0000cdcd  A15     : 0x00000000  SAR     : 0x00000017  EXCCAUSE: 0x00000000  
EXCVADDR: 0x00000000  LBEG    : 0x4000c46c  LEND    : 0x4000c477  LCOUNT  : 0xffffffff  

ELF file SHA256: 0000000000000000

Backtrace: 0x40116b78:0x3fedf570 ...(省略)
```
---
![输入图片说明](cover.JPG)  
 **一个能让学校生活更充实的设备**   

[介绍视频链接](https://www.bilibili.com/video/BV12g411b7A6)   
[立创开源链接](https://oshwhub.com/lxu0423/xiao-yuan-sheng-huo-zhu-shou-7-0)  
[51单片机热敏打印机](https://oshwhub.com/lxu0423/51-dan-pian-ji-i2c-re-min-da-yin-ji)  
[51单片机微型键盘](https://oshwhub.com/lxu0423/51-dan-pian-ji-wei-xing-jian-pan)  
[相关外设资料](https://gitee.com/lxy0423/school-assistant7-peripherals)  
如果想自己做一个，建议也看一下立创的README，略微不同
---
## 参数
主控：[ESP32-S3-N8R8](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.c9252e8diWMXIh&id=675349632310&_u=r3536jipc5f3)  
屏幕：[ST7789-2.8寸电容触摸](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.c9252e8diWMXIh&id=679358016598&_u=r3536jipc2e5)  
电源管理：AXP192，[参考购买链接](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.c9252e8diWMXIh&id=659507021534&_u=r3536jip9589)（其实都是翻新的定制芯片，不保证可用）  
I2S功放：NS4168  
GPIO：引出8个 < 40 的IO口  

## 功能

- 时间，精确到秒，可以以毫秒为单位偏移，并可调节振荡频率）
- 天气，每次更新时保存最近120小时天气、最近2小时分钟级降水、当前天气描述，实时计算月相
- 课程表管理及上下课提醒
- 连接WiFi（支持自定义校园网认证信息）
- 图片查看器
- 热敏打印电脑屏幕、录音
- 远程控制电脑执行cmd
- 与小米手环4通信，包括设置闹钟、设置提醒、同步天气、同步时间，并提供获取步数、心率等信息的API
- 小米手环4 NFC版的第三方语音助手，需要百度智能云的语音识别和UNIT的API——KEY，反正语音识别试用180天，嘻嘻
- 音乐播放（I2S）
- 音乐下载
- B站视频播放（mpeg1流）
- B站粉丝数、点赞数、私信数查看（需要扫码登录）
- Lua 解释器
- TF卡文件管理器
- 文本编辑器，支持外接键盘
- 支持IPv6，通过SLAAC自动获取IP
- 电池电源管理
- USB读卡器
- 可选每天验证密码

## 免责声明
DIY 有风险，因为是免费开源给大家，我只提供能力范围内的技术支持。  
下面这段话摘自GPL-3.0：  
  15. Disclaimer of Warranty.

  THERE IS **NO WARRANTY** FOR THE PROGRAM, TO THE EXTENT PERMITTED BY  
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT  
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY  
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,  
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR  
PURPOSE.  **THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM**  
**IS WITH YOU.** SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF  
**ALL NECESSARY SERVICING, REPAIR OR CORRECTION.**  

## 移植
嗯，这个项目想复刻比较困难，这里提供一个移植教程  
一定要复刻建议看一下发在立创的文档，希望对你有帮助  
PCB设计可以参考，但是AXP192估计买不到一模一样的，看运气吧。  
只支持ESP32S3，PSRAM要8MB或以上的，保证之后如果更新了可以直接用  
对应的硬件平台需要有一个DS3231实时时钟（便宜点的PCF8563也行）  
需要修改src/hal.cpp、src/main.cpp、include/hal/hal.h  
然后修改include目录下写着config的文件，都有对应的注释  
注意改完后把“example”去掉  
理论上这就可以了。

## 外设
[相关外设资料](https://gitee.com/lxy0423/school-assistant7-peripherals)  

## 安全
TCP都是明文传输，加上涉及到远程代码执行，还是要注意点。不过我这个在希沃白板上运行，怎么都是公用的，不怕别人攻击  
如果担心，可以删除python服务器的TCP功能  

## 获取小米手环连接密钥
[argrento/huami-token](https://github.com/argrento/huami-token)  
[这里也有一点](https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=baidu&wd=%E5%B0%8F%E7%B1%B3%E6%89%8B%E7%8E%AF%204%20%E5%AF%86%E9%92%A5%E8%8E%B7%E5%8F%96)  
[GadgetBridge](https://codeberg.org/Freeyourgadget/Gadgetbridge)  
## 网易云音乐API
[Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi)  
参考里面的vercel部署，省一个云主机/树莓派  
国内访问vercel可能被墙，绑定一个域名就可以用了  

## License
因为Arduino不可避免用到GPL的库，只能以GPL开源。另外，我没有授权任何商业行为。
