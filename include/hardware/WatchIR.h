#ifndef __WATCH_IR_H__
#define __WATCH_IR_H__
#include <A_Config.h>
#include <IRremoteESP8266.h>
#include <IRac.h>
#include <IRutils.h>

class WatchIR
{
private:
    /* data */
public:
    void enableIROut(bool en)      //是否允许红外发射
    {
        if(en)
        {
            pinMode(IR_LED, OUTPUT);
            digitalWrite(IR_LED, 0);
        }
        else
        {
            pinMode(IR_LED, INPUT);
            digitalWrite(IR_LED, 0);
        }
    }
    /**
     * @brief 将空调设置为初始状态
     */
    void ACModeReset();
};

extern WatchIR ir;
extern IRsend irsend;
extern IRac irac;
extern IRrecv irrecv;
#endif