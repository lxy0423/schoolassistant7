#ifndef __SCHOOLWIFI_H__
#define __SCHOOLWIFI_H__
#include "A_Config.h"

#include <URLCode.h>
#include <WiFi.h>
#include <HTTPClient.h>

class SchoolWiFi
{

private:
    const char *testHost = "example.com";
    const uint16_t testPort = 80;

public:
    const char *host = HOME_TCP_HOST;
    const uint16_t port = HOME_TCP_PORT;
    WiFiClient client;   // 用于TCP连接
    WiFiClient6 client6; // IPv6 Client
    HTTPClient http;     // 用于HTTP验证
    HTTPClient https;    // 只用于HTTPS(可能用不上)
    bool testConnection();
    bool connect();
    bool authenticate();
    bool connectWiFi()
    {
        connect();
        authenticate();
        return testConnection();
    }
    bool connectTCP()
    {
        for (uint8_t i = 0; i < 4; ++i)
        {
            client.setTimeout(3);
            client.connect(host, port, 1000);
            if (client.connected())
                return true;
        }
        return false;
    }
    bool uploadFile(const String path, const char *remote_name);
    bool download(const String url, File *file);
};

extern SchoolWiFi schoolWiFi;

#endif