#ifndef __WIFIMANAGER_H__
#define __WIFIMANAGER_H__
#include "A_Config.h"
#include <WiFi.h>

class WiFiManager
{

private:
    bool scanWiFiandConnect();
public:
    void begin();
    void save();
    void clearall();
    void add(const String ssid, const String pass);
    bool has(const String ssid);
    String getPassword(const String ssid);
    bool connectGUI();
    bool autoConnect();
    /**
     * @brief 获取IPv6局域网地址，在连接WiFi成功后调用
    */
    bool enableIPv6();
    ip6_addr_t ipv6local;
    ip6_addr_t ipv6global;
    const char *ipv6_to_str(ip6_addr_t *addr)
    {
        return ip6addr_ntoa(addr);
    }
    void ipv6_to_object(ip6_addr_t *addr, IPv6Address *obj)
    {
        *(obj) = addr->addr;
    }
};

extern WiFiManager WiFiMgr;

#endif