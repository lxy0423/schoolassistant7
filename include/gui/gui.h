#ifndef __MENU_H__
#define __MENU_H__
#include <Arduino.h>
#define GUI_ANIM_FLOATING_RANGE 8
#define GUI_ANIM_FLOATING_TIME 1000
/**
 * @brief 向下弹出动画
 * @param obj lvgl对象指针
 * @param distance 动画移动距离
 * @param time 动画持续时长
 * @param delay 动画开始前延时
 */
void lv_obj_push_down(lv_obj_t *obj, uint16_t distance = 24,
                      uint16_t time = 300, uint16_t waitBeforeStart = 0);

/**
 * @brief 向上弹出动画
 * @param obj lvgl对象指针
 * @param distance 动画移动距离
 * @param time 动画持续时长
 * @param delay 动画开始前延时
 */
void lv_obj_pop_up(lv_obj_t *obj, uint16_t distance = 30,
                   uint16_t time = 300, uint16_t waitBeforeStart = 0);

void lv_obj_fall_down(lv_obj_t *obj, uint16_t distance = 30,
                      uint16_t time = 300, uint16_t waitBeforeStart = 0);

/**
 * @brief 浮动动画
 * @param obj lvgl对象指针
 * @param waitBeforeStart 动画开始前延时
 */
void lv_obj_floating_add(lv_obj_t *obj, uint16_t waitBeforeStart = 0);

/**
 * @brief 移动到指定位置，渐慢动画
 * @param obj lvgl对象指针
 * @param x 目标位置x坐标
 * @param y 目标位置y坐标
 * @param time 动画持续时长
 * @param delay 动画开始前延时
 */
void lv_obj_move_anim(lv_obj_t *obj, int16_t x, int16_t y,
                      uint16_t time = 500, uint16_t waitBeforeStart = 0);
/**
 * @brief 创建并初始化菜单控件，清空菜单数组，但是不会立刻显示菜单。创建菜单第一步
 */
void menu_create(void);

/**
 * @brief 添加菜单项到数组
 * @param str 菜单项名称
 */
void menu_add(const char *str);

/**
 * @brief 处理菜单输入。清保证在调用此函数之前已调用menu_display()
 */
bool menu_handle();

/**
 * @brief 显示菜单函数, 只是显示，方便中途异步追加菜单项 需要一个循环来处理输入：
    while (1)
    {
        if(menu_handle())
        {
            break;
        }
    }    。然后用menu_get_selected()获取选择的项目
 */
void menu_display();

/**
 * @brief 显示菜单函数。显示菜单，并等待用户选择
 * @return 用户选择的菜单项，如果为0则为“返回”
 */
int16_t menu_show();

/**
 * @brief 简单的信息提示框
 * @param prompt 标题
 * @param msg 信息，即内容
 * @param auto_back 自动返回时间，单位ms，即如果用户不按下确定键，会在这么长时间后自动返回
 */
void msgbox(const char *prompt, const char *msg, uint32_t auto_back = 0);

/**
 * @brief 创建label
 *
 * @param parent 父对象
 * @param str label的初始内容
 * @param x x坐标
 * @param y y坐标
 * @param animation 是否需要渐入动画，默认不需要
 * @param anim_delay 动画延时启动
 * @return 创建好的label指针
 */
lv_obj_t *label(lv_obj_t *parent, const char *str, uint16_t x, uint16_t y, bool animation = false, uint16_t anim_delay = 0);

/**
 * @brief 创建按钮
 *
 * @param parent 父对象
 * @param text 按钮上显示的文本
 * @param x x坐标（默认左上角对齐
 * @param y y坐标
 * @param cb 回调函数
 * @return lv_obj_t* 创建好的按钮指针
 */
lv_obj_t *button(lv_obj_t *parent, const char *text, uint16_t x, uint16_t y, lv_event_cb_t cb);

/**
 * @brief 创建按钮
 *
 * @param parent 父对象
 * @param text 按钮上显示的文本
 * @param x x坐标（默认左上角对齐
 * @param y y坐标
 * @param var 按下标记，按钮按下时设置var为true
 * @return lv_obj_t* 创建好的按钮指针
 */
lv_obj_t *button(lv_obj_t *parent,const char *text, uint16_t x, uint16_t y, bool *var);

/**
 * @brief 在当前screen上创建一个全屏信息提示，注意只是创建一个并显示
 * @param icon 图标，只能从BIG_SYMBOL_*里面选一个
 * @param title 标题
 * @param str 信息，即内容
 * @param bg_color 背景颜色，可以从FULL_SCREEN_BG_*选图标对应的，也可以是别的
 * @return 创建的msgbox对象
 */
lv_obj_t *full_screen_msgbox_create(const char *icon, const char *title,
                                    const char *str,
                                    lv_color_t bg_color = lv_palette_main(LV_PALETTE_BLUE));

/**
 * @brief 删除全屏信息提示，渐出动画
 * @param mbox 之前创建的msgbox
 */
void full_screen_msgbox_del(lv_obj_t *mbox);

/**
 * @brief 等待用户按下确定键，然后删除全屏信息提示，渐出动画
 * @param mbox 之前创建的msgbox
 * @param auto_back 自动返回时间，单位ms，即如果用户不按下确定键，会在这么长时间后删除msgbox
 */
void full_screen_msgbox_wait_del(lv_obj_t *mbox, uint32_t auto_back = 0);

/**
 * @brief 在当前screen上创建一个全屏信息提示，等待用户按下确定并删除这个提示
 * @param icon 图标，只能从BIG_SYMBOL_*里面选一个
 * @param title 标题
 * @param str 信息，即内容
 * @param bg_color 背景颜色，可以从FULL_SCREEN_BG_*选图标对应的，也可以是别的
 * @param auto_back 自动返回时间，单位ms，即如果用户不按下确定键，会在这么长时间后删除msgbox
 * @return 创建的msgbox对象
 */
void full_screen_msgbox(const char *icon, const char *title,
                        const char *str,
                        lv_color_t bg_color = lv_palette_main(LV_PALETTE_BLUE),
                        uint32_t auto_back = 0);

/**
 * @brief
 * 倒计时到下一分钟0秒
 */
void countdown(void);

/**
 * @brief 信息提示-选择
 * @return true：确认，false：取消
 */
bool msgbox_yn(const char *str);

/**
 * 输入框——时间
 * @param str 提示字符串
 * @param value_pre 预设值，单位分钟，默认为0
 * @return 时间，单位分钟
 */
uint16_t msgbox_time(const char *str, uint16_t value_pre = 0);

/**
 * @brief 数字输入框
 *
 * @param str 提示字符串
 * @param max 最大输入数，超过后显示会自动变为最小数
 * @param min 最小输入数，低于此数显示会自动变为最大输入数
 * @param value_pre 预设值
 * @return 一个有符号整数，代表输入的值
 */
int msgbox_number(const char *str, int max, int min, int value_pre);

/**
 * @brief morse文字输入框，支持特殊字符
 * @param msg 提示字符串
 * @param multiline 允许多行
 * @param passwd 是否使用密码模式
 * @return 输入的字符串
 */
String msgbox_string(const char *msg, bool multiline = true, bool passwd = false, bool chinese = false);

/**
 * @brief 密码输入框(为兼容性考虑)
 * @return 输入的字符串
 */

String msgbox_passwd();

/**
 * @brief 显示toast提示
 * @param str 要显示的内容
*/
void lv_toast(const char *str);

//字体定义
extern const lv_font_t lv_font_chinese_16;
extern const lv_font_t num_32px;
extern const lv_font_t num_48px;
extern const lv_font_t num_64px;
extern const lv_font_t font_weather_32;        // weather字体：有雹中晴扬大云雨特冰浓雪多冻霾雷夹重严尘无阵暴伴浮沙雾小强阴度
extern const lv_font_t font_weather_num_24;    // 0123456789-/℃.:
extern const lv_font_t font_weather_symbol_48; // 0xf764,0xf773,0xf72e,0xf185,0xf070,0xf74f,0xf741,0xf763,0xf740,0xf73f,0xf73d,0xf0c2,0xf0e7,0xf2dc,0xf7cf
// extern const lv_font_t icon_64px;              //按顺序：0xf129,0xf00d, 0xf00c, 0xf128, 0xf023, 0xf0f3, 0xf021
extern const lv_font_t app_icon_28;
#define BIG_SYMBOL_INFO "\xEF\x84\xA9"
#define BIG_SYMBOL_QUESTION "\xEF\x84\xA8"
#define BIG_SYMBOL_LOCK "\xEF\x80\xA3"
#define BIG_SYMBOL_BELL "\x01\xFF"
#define BIG_SYMBOL_SYNC "\x02\xFF"
#define BIG_SYMBOL_CHECK "\x03\xFF"
#define BIG_SYMBOL_CROSS "\x04\xFF"
#define BIG_SYMBOL_UPLOAD "\x05\xFF"
#define BIG_SYMBOL_DOWNLOAD "\x06\xFF"
#define BIG_SYMBOL_WIFI "\x07\xFF"
#define BIG_SYMBOL_BELL_NUM 1
#define BIG_SYMBOL_SYNC_NUM 2
#define BIG_SYMBOL_CHECK_NUM 3
#define BIG_SYMBOL_CROSS_NUM 4
#define BIG_SYMBOL_UPLOAD_NUM 5
#define BIG_SYMBOL_DOWNLOAD_NUM 6
#define BIG_SYMBOL_WIFI_NUM 7

#define FULL_SCREEN_BG_INFO lv_palette_main(LV_PALETTE_BLUE)
#define FULL_SCREEN_BG_BELL lv_palette_main(LV_PALETTE_ORANGE)
#define FULL_SCREEN_BG_LOCK lv_palette_main(LV_PALETTE_ORANGE)
#define FULL_SCREEN_BG_CROSS LV_COLOR_MAKE(0xff, 0x6c, 0x5b)
#define FULL_SCREEN_BG_CHECK lv_palette_main(LV_PALETTE_GREEN)
#define FULL_SCREEN_BG_QUESTION lv_palette_main(LV_PALETTE_LIGHT_BLUE)
#define FULL_SCREEN_BG_SYNC LV_COLOR_MAKE(0x18, 0x96, 0xff)
#define FULL_SCREEN_BG_UPLOAD LV_COLOR_MAKE(0x18, 0x96, 0xff)
#define FULL_SCREEN_BG_DOWNLOAD LV_COLOR_MAKE(0x00, 0x8c, 0xff)
#define FULL_SCREEN_BG_WIFI LV_COLOR_MAKE(0x00, 0x8c, 0xff)
LV_IMG_DECLARE(img_gif_upload);
LV_IMG_DECLARE(img_gif_download);
LV_IMG_DECLARE(img_gif_wifi);
LV_IMG_DECLARE(img_gif_alarm);
LV_IMG_DECLARE(img_gif_ok);
LV_IMG_DECLARE(img_gif_fail);
LV_IMG_DECLARE(img_gif_loading);
LV_IMG_DECLARE(moon_000);
LV_IMG_DECLARE(moon_010);
LV_IMG_DECLARE(moon_020);
LV_IMG_DECLARE(moon_030);
LV_IMG_DECLARE(moon_040);
LV_IMG_DECLARE(moon_050);
LV_IMG_DECLARE(moon_060);
LV_IMG_DECLARE(moon_070);
LV_IMG_DECLARE(moon_080);
LV_IMG_DECLARE(moon_090);
LV_IMG_DECLARE(moon_100);
LV_IMG_DECLARE(moon_110);
LV_IMG_DECLARE(moon_120);
LV_IMG_DECLARE(moon_130);
LV_IMG_DECLARE(moon_140);
LV_IMG_DECLARE(moon_150);
LV_IMG_DECLARE(moon_160);
LV_IMG_DECLARE(moon_170);
LV_IMG_DECLARE(moon_180);
LV_IMG_DECLARE(moon_190);
LV_IMG_DECLARE(moon_200);
LV_IMG_DECLARE(moon_210);
LV_IMG_DECLARE(moon_220);
LV_IMG_DECLARE(moon_230);
LV_IMG_DECLARE(moon_240);
LV_IMG_DECLARE(moon_250);
LV_IMG_DECLARE(moon_260);
LV_IMG_DECLARE(moon_270);
LV_IMG_DECLARE(moon_280);
LV_IMG_DECLARE(moon_290);
LV_IMG_DECLARE(moon_300);
LV_IMG_DECLARE(moon_310);
LV_IMG_DECLARE(moon_320);
LV_IMG_DECLARE(moon_330);
LV_IMG_DECLARE(moon_340);
LV_IMG_DECLARE(moon_350);

#endif
