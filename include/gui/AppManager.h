#pragma once
#include <A_Config.h>
#include <list>
#include <stack>

class AppBase
{
private:
public:
    lv_obj_t *scr = NULL;                // App用的scr，在调用setup之前应自动初始化
    const char *name = "BaseApp";        // 一个标识符，唯一标识这个App
    const char *title = "BaseApp";       // 一个标题，显示在App列表中
    const char *description = "BaseApp"; // App描述，显示在App列表中
    const void *image = LV_SYMBOL_FILE;  // App图像，显示在App列表中，是一个36*36的图标
    int appID = 0;                       // AppID，唯一标识App
    bool _showInList = true;
    /**
     * @brief 初始化屏幕内容并初始化变量，此时scr_main已经可用，而且不需要注意互斥锁问题
     */
    virtual void setup(){};
    /**
     * 屏幕主循环
     */
    virtual void loop();
    /**
     * 释放资源（这里不用删除scr，只是释放类创建时创建的资源，不要在析构函数释放）
     */
    virtual void destruct(){};
    AppBase();
    ~AppBase();
};

class AppManager
{
private:
    /* data */
    std::stack<AppBase *> appStack;
    enum
    {
        APPMANAGER_NOOPERATION = 0,
        APPMANAGER_GOBACK = 1,
        APPMANAGER_GOTOAPP = 2
    } method;
    lv_obj_t *createAppScr();
    lv_obj_t *appselector = NULL;
    int validAppID = 1;

public:
    std::list<AppBase *> appList;
    AppBase *currentApp = NULL;
    int getAValidAppID()
    {
        return validAppID;
    }
    void increaseValidAppID()
    {
        validAppID++;
    }
    int getIDByName(const char *appName);
    void gotoApp(int appID);
    void gotoApp(const char *appName)
    {
        int appID = getIDByName(appName);
        gotoApp(appID);
    }
    void gotoDefaultApp()
    {
        gotoApp("clock");
    }
    void goBack();
    void showAppList();
    void update();
    String parameter = "";      //传递的参数，会在goto目标app的setup执行完后自动清空
    String result = "";         //传递的返回值，不会自动清空
};

extern AppManager appManager;
