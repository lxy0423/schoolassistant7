#ifndef __MICROKB_H__
#define __MICROKB_H__
#include <Arduino.h>
#include <Wire.h>

class MicroKB
{
private:
    const char MICROKB_ADDR = 0x10;
    char keyCodeToChar[60] =
        {
            ESC,
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '0',
            '-',
            '=',
            '\b',
            '`',
            'q',
            'w',
            'e',
            'r',
            't',
            'y',
            'u',
            'i',
            'o',
            'p',
            '[',
            ']',
            '\\',
            '\t',
            SHIFT,
            'a',
            's',
            'd',
            'f',
            'g',
            'h',
            'j',
            'k',
            'l',
            ';',
            '\'',
            UP,
            CTRL,
            '/',
            'z',
            'x',
            'c',
            'v',
            'b',
            'n',
            'm',
            ',',
            '.',
            LEFT,
            DOWN,
            RIGHT,
            DEL,
            ALT,
            ' ',
            '\n',
    };
    char keyCodeToCharShifted[60] =
        {
            ESC,
            '!',
            '@',
            '#',
            '$',
            '%',
            '^',
            '&',
            '*',
            '(',
            ')',
            '_',
            '+',
            '\b',
            '~',
            'Q',
            'W',
            'E',
            'R',
            'T',
            'Y',
            'U',
            'I',
            'O',
            'P',
            '{',
            '}',
            '|',
            '\t',
            SHIFT,
            'A',
            'S',
            'D',
            'F',
            'G',
            'H',
            'J',
            'K',
            'L',
            ':',
            '"',
            UP,
            CTRL,
            '?',
            'Z',
            'X',
            'C',
            'V',
            'B',
            'N',
            'M',
            '<',
            '>',
            LEFT,
            DOWN,
            RIGHT,
            DEL,
            ALT,
            ' ',
            '\n',
    };
    QueueHandle_t queue;
public:
    static const char DEL = 0x81;
    static const char CTRL = 0x82;
    static const char ALT = 0x83;
    static const char UP = 0x84;
    static const char DOWN = 0x85;
    static const char LEFT = 0x86;
    static const char RIGHT = 0x87;
    static const char ESC = 0x88;
    static const char SHIFT = 0x89;
    bool running = false;
    void begin();
    bool isConnected();
    char getChar(uint32_t timeout = portMAX_DELAY);
    void update();
    unsigned int available()
    {
        return uxQueueMessagesWaiting(queue);
    }
    bool isShift = false;
    bool isCtrl = false;
    bool isAlt = false;
    bool isESC = false;
};


extern MicroKB microKB;
#endif