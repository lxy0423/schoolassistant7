#ifndef __A_CONFIG__
#define __A_CONFIG__
#include "Config_Local.h" //在这里配置各种参数
#define LGFX_USE_V1
// #define AXP_RTC_INT_BOND
#define RTC_IS_DS3231
// #define SD_IS_SDMMC
#ifdef SD_IS_SDMMC
#define SDCARD SD_MMC
#else
#define SDCARD SD
#endif
/////////////////////////硬件IO连接-HW1.1/////////////////////
#define SDIO_CLK 6
#define SDIO_CMD 7
#define SDIO_D0 5
#define SDIO_D1 -1
#define SDIO_D2 -1
#define SDIO_D3 15
#define SD_SENSOR 4

#define I2C_SDA 14
#define I2C_SCL 21
#define AXP192_IRQ 13
#define RTC_IRQ 12

#define TOUCH_RST 46
#define LCD_DC 45
#define LCD_WR 48
#define LCD_RD 47
#define LCD_D0 0
#define LCD_D1 38
#define LCD_D2 39
#define LCD_D3 40
#define LCD_D4 41
#define LCD_D5 42
#define LCD_D6 2
#define LCD_D7 1
////////////////////////模块IO配置（可复用）/////////////////////////

#define I2C1_SDA 16
#define I2C1_SCL 17

#define I2S_LRCLK 16
#define I2S_BCLK 17
#define I2S_DATA 8

#define IR_LED 10

#define SD_MISO SDIO_D0
#define SD_MOSI SDIO_CMD
#define SD_SCLK SDIO_CLK
#define SD_CS SDIO_D3

#define screenWidth 320
#define screenHeight 240

#define AUDIO_I2S_DEVICE I2S_NUM_1

#define AUTO_SLEEP_TIME_DEFAULT 6000

#define BATTERY_1_VOLTAGE 3500.0
#define BATTERY_100_VOLTAGE 4140.0
#define PRINTER_BUFFER_SIZE 1024 * 1024
#define ESPNOW_FILE_LEN_PER_PKT 128

#define CONFIG_FILE_NAME "/config.json"

#define CONFIG_NTP_ADDR "ntp.ntsc.ac.cn" // NTP服务器
#define ESPNOW_FILE_TIMEOUT 3000         // ESPNow文件传输超时时间
#define ALARM_AUTO_STOP 60               // 闹钟自动停止时间

#include <Arduino.h>
#include <SPI.h>
#ifdef SD_IS_SDMMC
#include <SD_MMC.h>
#else
#include <SD.h>
#endif
#include <FS.h>
#include <SPIFFS.h>
#include <lvgl.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <WiFiClient6.h>
#include <Wire.h>
#include <axp20x.h>
#include "EspNowFloodingMesh.h"
#include "Macro.h"
#include "SettingsManager.h"
#include <Paperang.h>
#include <ThermalPrinter.h>
#include <moonPhase.h>
#include <DS3231.h>
#include <GT911.h>
#include <LovyanGFX.hpp>
#include "Audio.h" // https://github.com/schreibfaul1/ESP32-audioI2S

#include "MicroKB.h"
#include "hardware/WatchIR.h"
#include "hardware/hal.h"
#include "system/alarm.h"
#include "gui/gui.h"
#include "gui/AppManager.h"

#include "system/SchoolWiFi.h"
#include "system/WiFiManager.h"
#include "system/ESPNow.h"
#include "system/weather.h"
#include "system/WiFiManager.h"
#include "system/VideoPlayer.h"
#include "DebugHelper.h"
extern SettingsManager settings;
extern URLCode urlcode;

#endif