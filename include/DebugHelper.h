#pragma once
#include "A_Config.h"
#define DEBUG_ENTRY_MAX 60
typedef void (*debugFunction_t)(void);
typedef struct
{
    const char *name;
    debugFunction_t f;
} debugDesc_t;

class DebugHelper
{
private:
    debugDesc_t _list[DEBUG_ENTRY_MAX];
    uint16_t count = 0;

public:
    lv_obj_t *obj;
    void clear()
    {
        count = 0;
    }
    void showMenu(lv_obj_t *parent);
    void addEntry(const char *name, debugFunction_t f)
    {
        if (count >= DEBUG_ENTRY_MAX)
            assert(0);
        _list[count].f = f;
        _list[count].name = name;
        ++count;
    }
    void update();
};

#undef DEBUG_ENTRY_MAX