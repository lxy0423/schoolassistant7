#ifndef __CALLBACKS_H
#define __CALLBACKS_H
#include <Arduino.h>
#include <NimBLEDevice.h>

class ClientCallbacks : public NimBLEClientCallbacks
{
public:
	void onConnect(BLEClient *pClient)
	{
		LOG_PRINT("蓝牙设备已连接\n");
	}
	void onDisconnect(BLEClient *pClient)
	{
		LOG_PRINT("蓝牙设备已断开\n");
	}
	bool onConnParamsUpdateRequest(NimBLEClient *pClient, const ble_gap_upd_params *params)
	{
		return true;
	}
	uint32_t onPassKeyRequest()
	{
		return 123456;
	}
	void onAuthenticationComplete(ble_gap_conn_desc *desc)
	{
		if (!desc->sec_state.encrypted)
		{
			NimBLEDevice::getClientByID(desc->conn_handle)->disconnect();
			return;
		}
	}
	bool onConfirmPIN(uint32_t pin)
	{
		return true;
	}
};

#endif