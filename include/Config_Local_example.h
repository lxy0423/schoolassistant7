/*在这里配置各种参数*/
#ifndef __CONFIG_LOCAL_H__
#define __CONFIG_LOCAL_H__

#define ESPNOW_DEVICEID 0x8001
#define ESPNOW_MASTER_DEVICEID 0x1001
#define ESP_NOW_CHANNEL 1

#define DEVICENAME "SchoolAssistant"

// #define CMD_PASSWORD ""                        /*密码，为保证安全，每天需要输入一次，注释这一行可以禁用此功能*/
#define WIFI_SAVE_MAX 30                          /*防止nvs区溢出*/
#define CONFIG_DEFAULT_BRIGHTNESS 60              /*默认亮度*/
#define CONFIG_DEFAULT_VOLUME 50                  /*默认音量*/
#define CONFIG_NTP_OFFSET 8 * 3600                /*时区偏移*/
#define HOME_TCP_HOST "tcp.example.com"           /*TCP服务器地址*/
#define HOME_TCP_PORT 12345                       /*TCP服务器端口*/
#define HOME_IPV6_HOST "example.com"              /*IPv6服务器地址(BETA)*/
#define HOME_IPV6_PORT 12346                      /*IPv6服务器端口*/
#define HOME_TCP_VIDEO_HOST ""                    /*视频TCP服务器地址*/
#define HOME_TCP_VIDEO_PORT 12345                 /*视频TCP服务器端口*/
#define SCHOOL_WIFI_SSID "WIFISSID"               /*学校WiFi SSID*/
#define SCHOOL_WIFI_PASSWD "********"             /*学校WiFi密码*/
#define SCHOOL_WIFI_POST_DATA ""                  /*学校WiFi POST数据，联网后认证用*/
#define SCHOOL_WIFI_HOST "http://"                /*学校WiFi登录地址*/
#define WEATHER_LOCATION "116%2C39"               /*更新天气的位置*/
#define CLOUDMUSIC_DOMAIN "music.cyrilstudio.top" /*网易云音乐api地址*/
#define CHAT_GPT_API_KEY "sk-******"              /*CHATGPT API TOKEN*/
#define CHAT_GPT_API_URL "https://api.openai.com" /*CHATGPT API URL*/
#define CHAT_GPT_MODEL "gpt-3.5-turbo"            /*CHATGPT模型选择*/

#define MIBAND_KEY                                            \
    {                                                         \
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 \
    }                                                         /*小米手环连接密钥*/
#define MIBAND_ADDRESS "AA:BB:CC:DD:EE:FF"                    /*小米手环连接地址*/
#define BAIDU_CLIENT_ID "client_id=*****&client_secret=*****" /* 百度API-clientID和clientsecret*/
#define BAIDU_BOT_ID "S123456"                                /* 百度botid*/
#endif