#include "A_Config.h"

static lv_obj_t *arc_temperature;
static lv_obj_t *lbl_temperature;
static lv_obj_t *btn_onoff;
WatchIR watchIR;
static const char *models_str = "格力\n"
                                "美的\n"
                                "美的2";
static decode_type_t models[] = {
    KELVINATOR,
    COOLIX,
    MIDEA,
};

class AppIrRemote : public AppBase
{
private:
    /* data */
public:
    AppIrRemote()
    {
        name = "irremote";
        title = "红外遥控器";
        description = "简易红外遥控器，可以遥控空调";
        image = "\xEF\xA3\xB4";
    }

    void setup();
    void loop();
    void destruct();
};

static void event_onoff(lv_event_t *e)
{
    lv_obj_t *obj = lv_event_get_target(e);
    if (lv_obj_has_state(obj, LV_STATE_CHECKED))
    {
        irac.next.power = false;
        lv_label_set_text(lbl_temperature, "已关闭");
    }
    else
    {
        irac.next.power = true;
        lv_label_set_text_fmt(lbl_temperature, "%d", lv_arc_get_value(arc_temperature));
    }
}
static void event_tempchange(lv_event_t *e)
{
    irac.next.degrees = lv_arc_get_value(arc_temperature);
    if (lv_obj_has_state(btn_onoff, LV_STATE_CHECKED) == false)
        lv_label_set_text_fmt(lbl_temperature, "%d", lv_arc_get_value(arc_temperature));
}

static void event_model(lv_event_t *e)
{
    lv_obj_t *obj = lv_event_get_target(e);
    irac.next.protocol = models[lv_dropdown_get_selected(obj)];
}

static void event_mode(lv_event_t *e)
{
    lv_obj_t *obj = lv_event_get_target(e);
    irac.next.mode = (stdAc::opmode_t)lv_dropdown_get_selected(obj);
}

static void event_wind(lv_event_t *e)
{
    lv_obj_t *obj = lv_event_get_target(e);
    irac.next.fanspeed = (stdAc::fanspeed_t)lv_dropdown_get_selected(obj);
    if (irac.next.fanspeed == (stdAc::fanspeed_t)6)
    {
        irac.next.fanspeed = (stdAc::fanspeed_t)5;
        irac.next.turbo = true;
    }
    else
    {
        irac.next.turbo = false;
    }
}

static void event_open(lv_event_t *e)
{
    lv_obj_t *obj = lv_event_get_target(e);
    if (lv_obj_has_state(obj, LV_STATE_CHECKED))
    {
        lv_obj_set_style_text_font(lv_dropdown_get_list(obj), &lv_font_chinese_16, 0);
    }
}

static void event_send(lv_event_t *e)
{
    watchIR.enableIROut(true);
    irac.sendAc();
    watchIR.enableIROut(false);
}

void AppIrRemote::setup()
{
    LOCKLV();
    watchIR.ACModeReset();
    arc_temperature = lv_arc_create(scr);
    lv_obj_set_size(arc_temperature, 150, 150);
    lv_arc_set_rotation(arc_temperature, 135);
    lv_arc_set_bg_angles(arc_temperature, 0, 270);
    lv_arc_set_range(arc_temperature, 16, 30);
    lv_arc_set_value(arc_temperature, 26);
    lv_obj_align(arc_temperature, LV_ALIGN_TOP_RIGHT, -10, 10);
    lv_obj_add_event_cb(arc_temperature, event_tempchange, LV_EVENT_VALUE_CHANGED, NULL);

    btn_onoff = lv_btn_create(scr);
    lv_obj_add_flag(btn_onoff, LV_OBJ_FLAG_CHECKABLE);
    lv_obj_set_size(btn_onoff, 100, 100);
    lv_obj_set_style_radius(btn_onoff, LV_RADIUS_CIRCLE, 0);
    lv_obj_add_state(btn_onoff, LV_STATE_CHECKED);
    lv_obj_align_to(btn_onoff, arc_temperature, LV_ALIGN_CENTER, 0, 0);
    lv_obj_add_event_cb(btn_onoff, event_onoff, LV_EVENT_CLICKED, NULL);

    lbl_temperature = label(btn_onoff, "已关闭", 0, 0);
    lv_obj_center(lbl_temperature);

    lv_obj_t *dd = lv_dropdown_create(scr);
    lv_dropdown_set_options(dd, models_str);
    lv_obj_set_style_text_font(dd, &lv_font_chinese_16, 0);
    lv_obj_set_pos(dd, 10, 10);
    lv_obj_set_size(dd, 80, 40);
    lv_obj_add_event_cb(dd, event_model, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(dd, event_open, LV_EVENT_CLICKED, NULL);

    dd = lv_dropdown_create(scr);
    lv_obj_set_style_text_font(dd, &lv_font_chinese_16, 0);
    lv_obj_set_pos(dd, 10, 60);
    lv_obj_set_size(dd, 80, 40);
    lv_dropdown_set_options(dd, "自动\n"
                                "最小\n"
                                "小\n"
                                "中\n"
                                "大\n"
                                "最大\n"
                                "强劲");
    lv_obj_add_event_cb(dd, event_wind, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(dd, event_open, LV_EVENT_CLICKED, NULL);

    dd = lv_dropdown_create(scr);
    lv_obj_set_style_text_font(dd, &lv_font_chinese_16, 0);
    lv_obj_set_pos(dd, 10, 110);
    lv_obj_set_size(dd, 80, 40);
    lv_dropdown_set_options(dd, "自动\n"
                                "制冷\n"
                                "制热\n"
                                "干燥\n"
                                "送风");
    lv_obj_add_event_cb(dd, event_mode, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(dd, event_open, LV_EVENT_CLICKED, NULL);

    lv_obj_align(button(scr, "发送", 0, 0, event_send), LV_ALIGN_BOTTOM_RIGHT, -10, -10);
    hal.axpShortPress = false;
    UNLOCKLV();
}

void AppIrRemote::loop()
{

    if (hal.axpShortPress)
    {
        hal.axpShortPress = false;
        appManager.goBack();
        return;
    }
}

void AppIrRemote::destruct()
{
}

static AppIrRemote app;

/*
#include <A_Config.h>

void menu_maker_ac_control()
{
    decode_type_t protocol;
    protocol = (decode_type_t)hal.conf.getInt("actype");
start:
    ir.enableIROut(true);
    menu_create();
    menu_add("开");
    menu_add("关");
    menu_add("模式");
    menu_add("温度");
    menu_add("风速");
    menu_add("定时");
    menu_add("型号");
    menu_add("红外接收");
    uint16_t m;
    switch (menu_show(1))
    {
    case 0:
        break;
    case 1: //开
        irac.next.power = true;
        irac.sendAc();
        goto start;
        break;
    case 2: //关
        irac.next.power = false;
        irac.sendAc();
        goto start;
        break;
    case 3: //模式
        menu_create();
        menu_add("自动");
        menu_add("制冷");
        menu_add("制热");
        menu_add("干燥");
        menu_add("送风");
        m = menu_show(2);
        if (m)
        {
            --m;
            irac.next.mode = (stdAc::opmode_t)m;
        }
        irac.sendAc();
        goto start;
        break;
    case 4: //温度
        m = msgbox_number("请输入温度", 2, 0, 30, 16, 24);
        irac.next.degrees = m;
        irac.sendAc();
        goto start;
        break;
    case 5: //风速
        menu_create();
        menu_add("自动");
        menu_add("最小");
        menu_add("小");
        menu_add("中");
        menu_add("大");
        menu_add("最大");
        menu_add("强劲");
        m = menu_show(1);
        if (m)
        {
            --m;
            if (m == 6)
            {
                m = 5;
                irac.next.turbo = true;
            }
            else
            {
                irac.next.turbo = false;
            }
            irac.next.fanspeed = (stdAc::fanspeed_t)m;
            irac.sendAc();
        }
        goto start;
        break;
    case 6: //定时
        m = msgbox_time("请输入定时时间");
        irac.next.sleep = m;
        irac.sendAc();
        goto start;
        break;
    case 7: //型号
    {
        decode_type_t protocols[30];
        uint8_t tmp = 1;
        menu_create();
        for (int i = 1; i < 30; i++)
        {
            protocols[tmp] = (decode_type_t)i;
            if (irac.isProtocolSupported(protocols[tmp]))
            {
                menu_add(typeToString(protocols[tmp]).c_str());
                ++tmp;
            }
        }
        m = menu_show(1);
        if (m)
        {
            irac.next.protocol = protocols[m];
            protocol = protocols[m];
            hal.conf.setValue("actype", String(protocol));
            hal.conf.writeConfig();
            irac.sendAc();
        }
        goto start;
        break;
    }
    case 8: //自动查找型号
    {
        decode_results results;
        lv_obj_t *m = full_screen_msgbox_create(BIG_SYMBOL_INFO, "等待中", "等待接收红外信号");
        irrecv.enableIRIn();
        irrecv.setUnknownThreshold(12);
        for (uint16_t i = 0; i < 500; ++i)
        {
            vTaskDelay(100);
            if (hal.btnEnter.isPressedRaw())
            {
                full_screen_msgbox_del(m);
                break;
            }
            if (irrecv.decode(&results))
            {
                full_screen_msgbox_del(m);
                protocol = results.decode_type;
                if (protocol != UNKNOWN)
                {
                    hal.conf.setValue("actype", String(protocol));
                    hal.conf.writeConfig();
                }
                String result = "类型：";
                result += typeToString(protocol);
                result += "。信息：";
                result += IRAcUtils::resultAcToString(&results);
                msgbox("解码成功，已保存", result.c_str());
                break;
            }
        }
        irrecv.disableIRIn();
        goto start;
        break;
    }
    default:
        break;
    }
    ir.enableIROut(false);
}
*/