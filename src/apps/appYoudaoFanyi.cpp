#include "A_Config.h"
class AppYoudaoFanyi : public AppBase
{
private:
    /* data */
    bool translatereq = false;

public:
    AppYoudaoFanyi()
    {
        name = "youdaofanyi";
        title = "有道翻译";
        description = "一个简单的有道翻译工具";
        image = "\xEF\x86\xAB";
    }
    void setup();
    void loop();
    void destruct();
};

void AppYoudaoFanyi::setup()
{
    WiFiMgr.autoConnect();
    lv_obj_center(button(scr, "翻译", 0, 0, &translatereq));
}

void AppYoudaoFanyi::loop()
{
    AppBase::loop();
    if (translatereq)
    {
        if (WiFi.isConnected())
        {
            String url = "http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i=" + urlcode.urlencode(msgbox_string("请输入要翻译的内容", false, false, true));
            schoolWiFi.http.begin(url);
            if (schoolWiFi.http.GET() == 200)
            {
                String res = schoolWiFi.http.getString();
                DynamicJsonDocument doc(1024);
                deserializeJson(doc, res);
                schoolWiFi.http.end();
                msgbox("翻译结果", doc["translateResult"][0][0]["tgt"]);
            }
        }
        translatereq = false;
    }
}

void AppYoudaoFanyi::destruct()
{
    WiFi.disconnect(true);
}

static AppYoudaoFanyi app;
