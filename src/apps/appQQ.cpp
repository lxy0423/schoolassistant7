#include "A_Config.h"
class AppQQ : public AppBase
{
private:
    /* data */
public:
    AppQQ()
    {
        name = "qq";
        title = "QQ";
        description = "一个隐藏的QQ，需要配置QQ服务器，暂不开放";
        image = LV_SYMBOL_FILE;
        _showInList = false;
    }
    void setup();
    void loop();
    void destruct();
    bool imageUploadReq = false;
    char imageAddr[200];
};
static AppQQ app;
static lv_obj_t *txt_message;
static lv_obj_t *led_status;
static String message;

static void clear()
{
    String pack = "{\"method\":\"clear\",\"payload\":\"\"}";
    if (schoolWiFi.client.connected() == false)
        if (schoolWiFi.connectTCP() == false)
        {
            msgbox(LV_SYMBOL_CLOSE " 错误", "无法连接到服务器");
            return;
        }
    schoolWiFi.client.write(pack.c_str());
    schoolWiFi.client.readStringUntil('\n');
    lv_toast("已清空");
    message = "[空]";
}

static String reply(String s)
{
    String pack = "{\"method\":\"send\",\"payload\":\"";
    s.replace("\"", "\\\"");
    s.replace("\\", "\\\\");
    s.replace("\n", "\\n");
    pack += s;
    pack += "\"}";
    if (schoolWiFi.client.connected() == false)
        if (schoolWiFi.connectTCP() == false)
        {
            msgbox(LV_SYMBOL_CLOSE " 错误", "无法连接到服务器");
            return "无法连接到服务器";
        }
    schoolWiFi.client.write(pack.c_str());
    return schoolWiFi.client.readStringUntil('\n');
    lv_toast("发送成功");
}

static void updateMessage()
{
    if (schoolWiFi.client.connected() == false)
        if (schoolWiFi.connectTCP() == false)
        {
            msgbox(LV_SYMBOL_CLOSE " 错误", "无法连接到服务器");
            return;
        }
    schoolWiFi.client.write("{\"method\":\"get\",\"payload\":\"\"}");
    vTaskDelay(50);
    message = schoolWiFi.client.readStringUntil('\n');
    DynamicJsonDocument doc(4096);
    deserializeJson(doc, message);
    String s = doc["payload"].as<String>();
    if (message != s)
    {
        message = s;
        lv_textarea_set_text(txt_message, message.c_str());
    }
    doc.clear();
}

void AppQQ::setup()
{
    txt_message = lv_textarea_create(scr);
    lv_obj_set_size(txt_message, screenWidth - 20, screenHeight - 20);
    lv_obj_set_pos(txt_message, 10, 10);
    lv_obj_set_style_text_font(txt_message, &lv_font_chinese_16, 0);
    lv_obj_pop_up(txt_message);
    led_status = lv_led_create(scr);
    lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_RED));
    lv_led_set_brightness(led_status, 255);
    lv_obj_set_size(led_status, 6, 6);
    lv_obj_align(led_status, LV_ALIGN_TOP_RIGHT, -7, 7);
    hal.DoNotSleep = true;
    WiFiMgr.autoConnect();
}
uint32_t last_millis = 0;
void AppQQ::loop()
{
    if (WiFi.status() == WL_CONNECTED)
    {
        if (millis() - last_millis > 6000)
        {
            last_millis = millis();
            if (schoolWiFi.client.connected())
            {
                updateMessage();
            }
        }
        if (schoolWiFi.client.connected())
            lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_GREEN));
        else
            lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_BLUE));
    }
    else
        lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_RED));

    if (hal.axpShortPress)
    {
        menu_create();
        menu_add(LV_SYMBOL_WIFI " 连接WiFi");
        menu_add(LV_SYMBOL_DOWNLOAD " 更新");
        menu_add(LV_SYMBOL_UPLOAD " 回复");
        menu_add(LV_SYMBOL_TRASH " 清空记录");
        menu_add(LV_SYMBOL_IMAGE " 发送图片");
        menu_add(LV_SYMBOL_EJECT " 退出");
        uint16_t res = menu_show();
        switch (res)
        {
        case 1:
            WiFiMgr.autoConnect();
        case 2:
            lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_PURPLE));
            updateMessage();
            break;
        case 3:
        {
            String r = msgbox_string("输入你的回复：", true, false, true);
            if (r != "")
            {
                String p = "回复内容：\n";
                p += r;
                if (msgbox_yn(p.c_str()))
                {
                    lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_PURPLE));
                    lv_toast(reply(r).c_str());
                }
            }
            break;
        }
        case 4:
            if (msgbox_yn("是否清空记录"))
            {
                lv_led_set_color(led_status, lv_palette_main(LV_PALETTE_PURPLE));
                clear();
            }
            break;
        case 5:
        {
            char buffer[200];
            if (msgbox_yn("是否使用文件管理器查找文件？"))
            {
                appManager.parameter = "choose";
                appManager.gotoApp("filemanager");
                imageUploadReq = true; // 暂存状态
                hal.axpShortPress = false;
                return;
            }
            else
            {
                int fileid = msgbox_number("请输入图片ID", 9999, 0, 0);
                snprintf(imageAddr, 200, "/PHOTO/PICT%04d.jpg", fileid);
                imageUploadReq = true;
            }
            hal.axpShortPress = false;
            return;
            break;
        }
        case 6:
        {
            hal.DoNotSleep = false;
            WiFi.disconnect(true);
            appManager.goBack();
            break;
        }
        default:
            break;
        }
        hal.axpShortPress = false;
    }
    if (imageUploadReq)
    {
        imageUploadReq = false;
        if (appManager.result != "")
        {
            strcpy(imageAddr, appManager.result.c_str());
            appManager.result = "";
        }
        Serial.println(imageAddr);
        if (schoolWiFi.client.connected() == false)
            if (schoolWiFi.connectTCP() == false)
            {
                msgbox(LV_SYMBOL_CLOSE " 错误", "无法连接到服务器");
                return;
            }
        if (schoolWiFi.uploadFile(imageAddr, "image.jpg"))
        {
            schoolWiFi.client.write("{\"method\":\"sendImg\",\"payload\":\"\"}");
            schoolWiFi.client.readStringUntil('\n');
            lv_toast("发送成功");
        }
        else
        {
            lv_toast("文件上传失败");
        }
    }
}

void AppQQ::destruct()
{
}
