#include "A_Config.h"

static lv_obj_t *obj_weather_rain;
static lv_obj_t *chart_rain;
static lv_obj_t *label_rain;

void tile_weather_rain_update()
{
    lv_chart_refresh(chart_rain);
    lv_label_set_text(label_rain, weather.desc1);
    lv_label_ins_text(label_rain, LV_LABEL_POS_LAST, weather.desc2);
}

void tile_weather_rain_create(lv_obj_t *tile)
{
    obj_weather_rain = lv_obj_create(tile);
    lv_obj_set_size(obj_weather_rain, 208, 114);
    lv_obj_set_pos(obj_weather_rain, 108, 4);
    lv_obj_set_style_pad_all(obj_weather_rain, 4, 0);
    chart_rain = lv_chart_create(obj_weather_rain);
    lv_obj_set_size(chart_rain, lv_pct(100), 80);
    lv_obj_align(chart_rain, LV_ALIGN_TOP_MID, 0, 0);
    lv_chart_set_range(chart_rain, LV_CHART_AXIS_PRIMARY_Y, 0, 400);
    int16_t m = 0;
    for (int8_t i = 0; i < 120; ++i)
    {
        if (weather.rain[i] > m)
        {
            m = weather.rain[i];
        }
    }
    if (m > 400)
        lv_chart_set_range(chart_rain, LV_CHART_AXIS_PRIMARY_Y, 0, m);
    lv_obj_set_style_size(chart_rain, 0, LV_PART_INDICATOR);
    lv_chart_series_t *ser = lv_chart_add_series(chart_rain, lv_palette_main(LV_PALETTE_BLUE), LV_CHART_AXIS_PRIMARY_Y);
    lv_chart_set_point_count(chart_rain, 120); //点数
    lv_chart_set_ext_y_array(chart_rain, ser, (lv_coord_t *)weather.rain);

    label_rain = lv_label_create(obj_weather_rain);
    lv_obj_set_style_text_font(label_rain, &lv_font_chinese_16, 0);
    lv_label_set_long_mode(label_rain, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_set_size(label_rain, lv_pct(100), 22);
    lv_obj_align(label_rain, LV_ALIGN_BOTTOM_MID, 0, 0);
    lv_label_set_text(label_rain, weather.desc2);
}