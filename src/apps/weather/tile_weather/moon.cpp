#include "A_Config.h"
#include <moonPhase.h>

static lv_obj_t *obj_weather_moon;
static lv_obj_t *img_moon;
moonPhase mp;

lv_img_dsc_t const *moon_imgs[] = {
    &moon_000,
    &moon_010,
    &moon_020,
    &moon_030,
    &moon_040,
    &moon_050,
    &moon_060,
    &moon_070,
    &moon_080,
    &moon_090,
    &moon_100,
    &moon_110,
    &moon_120,
    &moon_130,
    &moon_140,
    &moon_150,
    &moon_160,
    &moon_170,
    &moon_180,
    &moon_190,
    &moon_200,
    &moon_210,
    &moon_220,
    &moon_230,
    &moon_240,
    &moon_250,
    &moon_260,
    &moon_270,
    &moon_280,
    &moon_290,
    &moon_300,
    &moon_310,
    &moon_320,
    &moon_330,
    &moon_340,
    &moon_350,
};

void tile_weather_moon_update()
{
    moonData_t moon; // variable to receive the data
    DateTime dt(hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
    moon = mp.getPhase(dt.unixtime() - CONFIG_NTP_OFFSET);

    uint16_t angle = moon.angle / 10;
    if(angle >= 36)angle = 0;
    
    lv_img_set_src(img_moon, moon_imgs[angle]);
}

void tile_weather_moon_create(lv_obj_t *tile)
{
    obj_weather_moon = lv_obj_create(tile);
    lv_obj_set_size(obj_weather_moon, 100, 114);
    lv_obj_set_pos(obj_weather_moon, 216, 122);
    lv_obj_set_style_bg_color(obj_weather_moon, lv_color_black(), 0);
    lv_obj_clear_flag(obj_weather_moon, LV_OBJ_FLAG_SCROLLABLE);
    img_moon = lv_img_create(obj_weather_moon);
    lv_obj_align(img_moon, LV_ALIGN_CENTER, 0, 0);
    
}