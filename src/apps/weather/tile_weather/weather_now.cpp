#include "A_Config.h"

static lv_obj_t *obj_weather_now;
static lv_obj_t *icon_weather;
static lv_obj_t *lbl_con;
static lv_obj_t *lbl_temp;

void tile_weather_weather_now_update(uint8_t month, uint8_t date, uint8_t hour)
{
    auto w = weather.getWeather(month, date, hour);
    if (w)
    {
        lv_label_set_text(icon_weather, weather_icons[w->weathernum]);
        lv_label_set_text(lbl_con, weather_names[w->weathernum]);
        char tmp[30];
        sprintf(tmp, "%.1f ℃", (float)(w->temperature / 10));
        lv_label_set_text(lbl_temp, tmp);
    }
    else
    {
        lv_label_set_text(lbl_con, "");
        lv_label_set_text(icon_weather, "");
        lv_label_set_text(lbl_temp, "--");
    }
}
void tile_weather_weather_now_create(lv_obj_t *tile)
{
    obj_weather_now = lv_obj_create(tile);
    lv_obj_set_size(obj_weather_now, 100, 114);
    lv_obj_set_pos(obj_weather_now, 4, 4);
    lv_obj_set_style_bg_color(obj_weather_now, lv_palette_main(LV_PALETTE_BLUE), 0);
    lv_obj_set_style_pad_all(obj_weather_now, 4, 0);

    icon_weather = lv_label_create(obj_weather_now);
    lv_obj_set_size(icon_weather, LV_SIZE_CONTENT, LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(icon_weather, &font_weather_symbol_48, 0);
    lv_obj_set_style_text_color(icon_weather, lv_color_white(), 0);
    lv_obj_align(icon_weather, LV_ALIGN_TOP_MID, 0, 0);

    lbl_con = lv_label_create(obj_weather_now);
    lv_obj_set_style_text_font(lbl_con, &font_weather_32, 0);
    lv_obj_set_style_text_color(lbl_con, lv_color_white(), 0);
    lv_obj_align(lbl_con, LV_ALIGN_BOTTOM_MID, 0, -22);

    lbl_temp = lv_label_create(obj_weather_now);
    lv_obj_set_style_text_font(lbl_temp, &font_weather_num_24, 0);
    lv_obj_set_style_text_color(lbl_temp, lv_color_white(), 0);
    lv_obj_align(lbl_temp, LV_ALIGN_BOTTOM_MID, 0, 0);
}