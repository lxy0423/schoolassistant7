#include "A_Config.h"
class AppWeather : public AppBase
{
private:
    /* data */
public:
    AppWeather()
    {
        name = "weather";
        title = "天气";
        description = "查看今日天气、降水、月相";
        image = "\xEF\x9D\xA3";
    }
    void setup();
    void loop();
    void destruct();
};

void tile_weather_rain_create(lv_obj_t *tile);
void tile_weather_48h_create(lv_obj_t *tile);
void tile_weather_weather_now_create(lv_obj_t *tile);
void tile_weather_moon_create(lv_obj_t *tile);

void tile_weather_weather_now_update(uint8_t month, uint8_t date, uint8_t hour);
void tile_weather_48h_update(uint8_t month, uint8_t date, uint8_t hour);
void tile_weather_rain_update();
void tile_weather_moon_update();

uint8_t weather_last_sec = 99;
void AppWeather::setup()
{
    weather_last_sec = 99;
    tile_weather_weather_now_create(scr);
    tile_weather_48h_create(scr);
    tile_weather_rain_create(scr);
    tile_weather_moon_create(scr);
}

void AppWeather::loop()
{
    if (hal.rtc.sec != weather_last_sec)
    {
        weather_last_sec = hal.rtc.sec;
        tile_weather_weather_now_update(hal.rtc.month, hal.rtc.day, hal.rtc.hour);
        tile_weather_48h_update(hal.rtc.month, hal.rtc.day, hal.rtc.hour);
        tile_weather_rain_update();
        tile_weather_moon_update();
    }
    if (hal.axpShortPress)
    {
        hal.axpShortPress = false;
        appManager.goBack();
        return;
    }
}

void AppWeather::destruct()
{
}

static AppWeather app;