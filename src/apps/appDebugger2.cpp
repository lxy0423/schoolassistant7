#include "A_Config.h"
class AppDebugger2 : public AppBase
{
private:
    /* data */
    uint8_t count = 0;
    lv_obj_t *lbl_sysinfo;
    char strInfo[300];

public:
    AppDebugger2()
    {
        name = "debugger2";
        title = "开发人员选项2";
        description = "开发人员选项（新版）";
        image = "\xEF\x97\xBC";
    }
    void setup();
    void loop();
    void destruct();
};
static AppDebugger2 app;
static DebugHelper debug;

static bool download_music_local(String keyword)
{
    bool getRandom = false;
    String choiceID;
    String result;
    HTTPClient http;
    DynamicJsonDocument doc(10240);
    String url = "https://" CLOUDMUSIC_DOMAIN "/search?limit=3&keywords=";
    if (keyword == "")
    {
        url = "https://api.uomg.com/api/rand.music?sort=热歌榜&format=json";
        getRandom = true;
    }
    else
    {
        url += urlcode.urlencode(keyword);
    }
    http.begin(url);
    int code = http.GET();
    if (code != 200)
    {
        lv_toast("错误：无法连接HTTPS服务器");
        Serial.printf("HTTP CODE: %d\n", code);
        http.end();
        return false;
    }
    result = http.getString();
    deserializeJson(doc, result);

    if (getRandom == false)
    {
        uint16_t offset = 0;
        while (1)
        {
            if (doc["code"] == 200)
            {
                int ids[3];
                uint8_t i;
                String songNames[3];
                menu_create();
                for (i = 0; i < 3; ++i)
                {
                    ids[i] = doc["result"]["songs"][i]["id"];
                    if (ids[i] == 0)
                        break;
                    songNames[i] = doc["result"]["songs"][i]["name"].as<String>() + String("-") + doc["result"]["songs"][i]["ar"][0]["name"].as<String>();
                    menu_add(songNames[i].c_str());
                }
                menu_add(LV_SYMBOL_NEXT " 下一页");
                doc.clear();
                uint8_t choice = menu_show();
                if (choice == 0)
                {
                    lv_toast("已取消");
                    http.end();
                    return false;
                }
                else if (choice == i + 1) // 如果是下一页
                {
                    offset += 3;
                    String url_suffix = "&offset=" + String(offset);
                    result = "";
                    doc.clear();
                    http.begin(url + url_suffix);
                    int code = http.GET();
                    if (code != 200)
                    {
                        lv_toast("错误：无法连接HTTPS服务器");
                        Serial.printf("HTTP CODE: %d\n", code);
                        http.end();
                        return false;
                    }
                    result = http.getString();
                    deserializeJson(doc, result);
                    continue;
                }
                choiceID = String(ids[choice - 1]);
                keyword = songNames[choice - 1];
                break;
            }
            else
            {
                lv_toast("搜索：JSON不符合预期");
                http.end();
                return false;
            }
        }
    }
    else
    {
        if (doc["code"] == 1)
        {
            url = doc["data"]["url"].as<String>();
            int first_id = url.indexOf("id=") + 3;
            choiceID = url.substring(first_id, url.indexOf("\"", first_id));
            keyword = doc["data"]["name"].as<String>() + String("-") + doc["data"]["artistsname"].as<String>();
        }
        else
        {
            lv_toast("随机歌曲获取失败");
            http.end();
            return false;
        }
    }
    url = "https://" CLOUDMUSIC_DOMAIN "/song/url?id=";
    url += String(choiceID);
    http.begin(url);
    if (http.GET() != 200)
    {
        lv_toast("获取歌曲下载地址失败");
        http.end();
        return false;
    }
    result = http.getString();
    deserializeJson(doc, result);
    if (doc["code"] == 200)
    {
        url = doc["data"][0]["url"].as<String>();
        if (msgbox_yn((String("是否下载：") + keyword).c_str()) == true)
        {
            File fmusic = SDCARD.open(String("/m/") + keyword + String(".mp3"), "w");
            if (!fmusic)
            {
                lv_toast("无法创建文件");
                http.end();
                return false;
            }
            if (schoolWiFi.download(url, &fmusic))
                lv_toast("下载成功");
            else
                lv_toast("下载失败");
        }
    }
    else
    {
        lv_toast("下载：JSON不符合预期");
        http.end();
        return false;
    }
    http.end();
    return true;
}

static char ipaddr[50] = {0};
static bool receivedip;
static void IP_RECV_CB(const uint8_t *pkt, uint8_t size, uint16_t from)
{
    strncpy(ipaddr, (const char *)pkt, 50);
    receivedip = true;
}
static void update_myip()
{
    if (strcmp(ipaddr, "") != 0)
    {
        settings.setChar("serverIP", ipaddr);
    }
}
static void testLatestFunc()
{
    if (WiFiMgr.autoConnect() == false)
        return;
    String v = "{\"method\":\"bilibili\",\"payload\":\"";
    v += msgbox_string("请输入av或bv号");
    v += "\"}";
    if (schoolWiFi.connectTCP())
    {
        schoolWiFi.client.write(v.c_str());
        String res = schoolWiFi.client.readStringUntil('\n');
        if (res.indexOf("true") != -1)
        {
            // 成功打开服务
            schoolWiFi.client.stop();
            lv_toast("服务端已启动，正在连接...");
            delay(2000);
            schoolWiFi.client.connect(HOME_TCP_VIDEO_HOST, HOME_TCP_VIDEO_PORT);
            if (schoolWiFi.client.connected())
            {
                videoPlayer.play();
            }
            schoolWiFi.client.stop();
        }
        else
        {
            Serial.println(res);
        }
    }
}
static void getLatestIP()
{
    receivedip = false;
    ESPNow.start();
    bool b = ESPNow.sendCmd(CMD_GETIP, "");
    if (b == false)
    {
        msgbox("错误", "接收IP地址失败-服务器不在范围内");
        ESPNow.RecvCB = NULL;
        ESPNow.end();
        return;
    }
    uint8_t i = 0;
    ESPNow.RecvCB = IP_RECV_CB;
    while (receivedip == false)
    {
        delay(10);
        ++i;
        if (i > 100)
        {
            msgbox("错误", "接收IP地址失败-指令未收到回复（对方已接收）");
            ESPNow.RecvCB = NULL;
            ESPNow.end();
            return;
        }
    }
    ESPNow.RecvCB = NULL;
    ESPNow.end();
    update_myip();
}
static void connectWiFi()
{
    hal.DoNotSleep = true;
    WiFiMgr.connectGUI();
    update_myip();
}
static void connectTCP()
{
    if (WiFiMgr.autoConnect())
    {
        if (WiFi.localIPv6()[0] == 0)
            WiFiMgr.enableIPv6();
        schoolWiFi.client6.setTimeout(2);
        schoolWiFi.client6.connect(ipaddr, HOME_TCP_PORT, 2000);
        if (schoolWiFi.client6.connected())
        {
            msgbox("提示", "TCP服务连接成功！");
            settings.writeSettings(CONFIG_FILE_NAME);
        }
        else
        {
            msgbox("错误", "无法连接TCP服务");
        }
        schoolWiFi.client6.stop();
    }
}
static void sendCommand()
{
    if (WiFi.isConnected())
    {
        if (schoolWiFi.client6.connected() == false)
        {
            schoolWiFi.client6.connect(ipaddr, HOME_TCP_PORT, 2000);
            if (schoolWiFi.client6.connected() == false)
                return;
        }
        String cmd = msgbox_string("请输入cmd指令", false);
        String out = "";
        DynamicJsonDocument doc(1024);
        doc["method"] = "cmd";
        doc["payload"] = cmd;
        serializeJson(doc, out);
        if (schoolWiFi.client6.connected() == false)
        {
            schoolWiFi.client6.connect(ipaddr, HOME_TCP_PORT, 2000);
            if (schoolWiFi.client6.connected() == false)
                return;
        }
        schoolWiFi.client6.write(out.c_str());
        schoolWiFi.client6.flush();
        schoolWiFi.client6.stop();
        msgbox("提示", "指令已发送");
    }
}
static void inputIP()
{
    String s = msgbox_string("请输入IP地址或主机名", false);
    strncpy(ipaddr, s.c_str(), 50);
}
static void downloadMusic()
{
    if (WiFi.isConnected() == false)
    {
        if (WiFiMgr.autoConnect() == false)
            return;
    }
    hal.DoNotSleep = true;
    download_music_local(msgbox_string("搜索歌曲", false, false, true));
}
static void enableSleep()
{
    lv_toast("已经允许自动休眠，即使WiFi已连接");
    hal.DoNotSleep = false;
}
static void testIPv6()
{
    if (WiFi.isConnected() == false)
        WiFiMgr.autoConnect();
    if (WiFi.localIPv6()[0] == 0)
        WiFiMgr.enableIPv6();

    schoolWiFi.client6.connect(HOME_IPV6_HOST, HOME_IPV6_PORT, 3000);
    if (schoolWiFi.client6.connected())
    {
        schoolWiFi.client6.write("{\"method\":\"count\",\"payload\":\"\"}");
        lv_toast(schoolWiFi.client6.readStringUntil('\n').c_str());
    }
    else
    {
        lv_toast("TCPv6 连接失败");
    }
    schoolWiFi.client6.stop();
}
static void syncTime()
{
    DateTime dt1(hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
    timeval tv;
    tv.tv_sec = dt1.unixtime() - (CONFIG_NTP_OFFSET);
    tv.tv_usec = 0;
    settimeofday(&tv, NULL);
    uint8_t pkt[10];
    pkt[0] = 1;
    memcpy(pkt + 1, &tv, sizeof(tv));
    ESPNow.start();
    espNowFloodingMesh_disableTimeDifferenceCheck();
    if (ESPNow.sendAndWaitAck(pkt, 9, 0x8002) == true)
    {
        lv_toast("发送成功");
    }
    else
    {
        lv_toast("发送失败");
    }
    ESPNow.end();
}
static void sendAlarm()
{
    uint8_t pkt[1];
    pkt[0] = 2;
    ESPNow.start();
    espNowFloodingMesh_disableTimeDifferenceCheck();
    if (ESPNow.sendAndWaitAck(pkt, 1, 0x8002) == true)
    {
        File f = SPIFFS.open("/alarm.bin", "r");
        delay(200);
        if (f)
        {
            if (ESPNow.sendFile(&f, 0x8002))
                lv_toast("发送成功");
            else
                lv_toast("发送失败");
            f.close();
        }
        else
        {
            lv_toast("文件打开失败");
        }
    }
    else
    {
        lv_toast("发送失败");
    }
    ESPNow.end();
}
static void powerOFFInk()
{
    uint8_t pkt[1];
    pkt[0] = 3;
    if (msgbox_yn("是否确认关机？"))
    {
        ESPNow.start();
        espNowFloodingMesh_disableTimeDifferenceCheck();
        if (ESPNow.sendAndWaitAck(pkt, 1, 0x8002) == true)
        {
            lv_toast("成功");
        }
        else
        {
            lv_toast("失败");
        }
        ESPNow.end();
    }
}

void AppDebugger2::setup()
{
    debug.clear();
    lbl_sysinfo = label(scr, "系统信息", 160, 0);
    lv_obj_set_width(lbl_sysinfo, 160);
    debug.addEntry("测试最新功能", testLatestFunc);
    debug.addEntry("获取白板IP", getLatestIP);
    debug.addEntry("选择WiFi", connectWiFi);
    debug.addEntry("连接白板TCP", connectTCP);
    debug.addEntry("执行CMD", sendCommand);
    debug.addEntry("输入IP", inputIP);
    debug.addEntry("音乐下载器", downloadMusic);
    debug.addEntry("允许屏幕关闭", enableSleep);
    debug.addEntry("同步Ink时间", syncTime);
    debug.addEntry("发送闹钟文件", sendAlarm);
    debug.addEntry("测试IPv6", testIPv6);
    debug.addEntry("关闭Ink", powerOFFInk);
    debug.showMenu(scr);
    hal.axpShortPress = false;
    String last_ip = settings.getChar("serverIP", "");
    strcpy(ipaddr, last_ip.c_str());
    update_myip();
}

void AppDebugger2::loop()
{
    count = count + 1;
    if (count >= 10)
    {
        count = 0;
        sprintf(strInfo, "堆内存:%u KiB\nOSPI内存:%u KiB\n运行时间:%u s\nCPU频率:%u MHz\nFlash频率:%u MHz\nFlash容量:%u MiB\n本机IP:%s\n%s\n白板IP:%s\n",
                heap_caps_get_free_size(MALLOC_CAP_INTERNAL) / 1024,
                ESP.getFreePsram() / 1024,
                millis() / 1000,
                ESP.getCpuFreqMHz(),
                ESP.getFlashChipSpeed() / 1000000,
                ESP.getFlashChipSize() / 1024 / 1024,
                WiFi.localIP().toString().c_str(),
                WiFiMgr.ipv6_to_str(&WiFiMgr.ipv6global),
                ipaddr);
        lv_label_set_text(lbl_sysinfo, strInfo);
    }
    delay(100);
    debug.update();
    AppBase::loop();
}

void AppDebugger2::destruct()
{
    WiFi.disconnect(true);
    hal.DoNotSleep = false;
}
