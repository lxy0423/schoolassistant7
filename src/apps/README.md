# 应用程序目录  

## 如何添加自己的应用？
---
如果要添加新的应用，需复制一份app_template.cpp.bak，修改后缀名为cpp，然后把里面的类名AppTemplate修改为一个和其它不一样的  
之后配置APP的构造函数，name要设置为独一无二的标识，必须设置为与其它不同  
其它的按照注释修改即可  


app会在启动后自动加入app列表，无需手动添加  
APPID启动后会自动分配，不用管它  

## 应用生命周期
---
setup会在scr创建后运行，loop会在scr在前台运行时运行，destruct会在scr即将被删除时运行，scr是App运行时的scr，由系统自动创建与删除  

## APP "看到的" 流程：  
---
```cpp
    scr = lv_obj_create(NULL);
    setup();
    while(SOME_CONDITION)
        loop();
    destruct();
    lv_obj_del(scr);
```

## APP图标设置  
---
image可以设置任何图像源`LV_IMG_CF_RAW_ALPHA`，也可以设置为FontAwesome图标字体，下面是已经用到的图标，如果需要添加自己的字体，请在下面这里按照格式注明它的Unicode代码和它的用途

[Unicode转UTF-8](https://www.cogsci.ed.ac.uk/~richard/utf-8.cgi?input=f763&mode=hex)  
[FontAwesome](https://fontawesome.com)  
[Online Font Converter](https://lvgl.io/tools/fontconverter)  


```cpp
字体名称：app_icon_28  

用Online Font Converter字体生成时用这一行： 0xf017,0xf5fc,0xf07c,0xf03e,0xf8f4,0xf293,0xf8e4,0xf013,0xf15b,0xf1ab,0xf763,0xf04b,0xf31c,0xf11c

默认：f15b LV_SYMBOL_FILE
时钟：f017  "\xEF\x80\x97"  
开发者选项：f5fc "\xEF\x97\xBC"  
文件浏览器：f07c "\xEF\x81\xBC"  
图像查看器：f03e "\xEF\x80\xBE"  
红外遥控器：f8f4 "\xEF\xA3\xB4"  
小米运动：f293 "\xEF\x8A\x93"  
音乐播放器：f8e4 "\xEF\xA3\xA4"  
设置：f013 "\xEF\x80\x93"  
文本编辑器：f31c "\xEF\x8C\x9C"  
天气：f763 "\xEF\x9D\xA3"  
有道翻译：f1ab "\xEF\x86\xAB"  
BiliBili f04b "\xEF\x81\x8B"  
Lua lua_logo
键盘 f11c "\xEF\x84\x9C"

```