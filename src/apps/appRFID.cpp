#include "A_Config.h"
#include <Adafruit_PN532.h>
static lv_obj_t *lbl_info = NULL;
static lv_obj_t *btn_write = NULL;
static bool writecardreq = false;
class AppPN532 : public AppBase
{
private:
    /* data */
    Adafruit_PN532 nfc = Adafruit_PN532(-1, -1, &Wire1);
    bool unready = false;
    bool rfidAuth(uint8_t *uid, int block);

public:
    AppPN532()
    {
        name = "nfc";
        title = "NFC";
        description = "NFC卡工具";
        image = LV_SYMBOL_FILE;
    }
    void setup();
    void loop();
    void destruct();
};
static AppPN532 app;
static void rfidRead()
{
}
static void rfidWrite()
{
}
void AppPN532::setup()
{
    Wire1.end();
    Wire1.setPins(10, 11);
    Wire1.begin();
    hal.DoNotSleep = true;
    nfc.begin();
    unready = false;
    uint32_t versiondata = nfc.getFirmwareVersion();
    if (!versiondata)
    {
        msgbox("错误", "请检查PN532连接");
        unready = true;
        return;
    }
    else
    {
        lbl_info = label(scr, "PN532信息", 10, 10);
        lv_label_set_text_fmt(lbl_info, "芯片：PN5%02x\n固件：%d.%d\n接口：I2C\n  请放卡...", (versiondata >> 24) & 0xFF, (versiondata >> 16) & 0xFF, (versiondata >> 8) & 0xFF);
        lv_obj_set_width(lbl_info, 300);
        lv_obj_set_style_text_align(lbl_info, LV_TEXT_ALIGN_CENTER, 0);
        lv_obj_center(lbl_info);
        btn_write = button(scr, "写卡", 100, 190, &writecardreq);
    }
}
#define CARD_START 16
#define CARD_END 23
uint8_t rfid_data[CARD_END - CARD_START + 1][16]; // Array to store block data during reads
bool AppPN532::rfidAuth(uint8_t *uid, int block)
{
    bool success;
    uint8_t keyuniversal[][6] = {
//        {0x08, 0x29, 0xad, 0xef, 0x20, 0xcb},
        {0x20, 0x08, 0xaf, 0xde, 0xcb, 0x29},
//        {0xbc, 0x3f, 0x46, 0x96, 0x1a, 0x82},
//        {0x23, 0x8a, 0xff, 0x00, 0xa7, 0xc9},
//        {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
//        {0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
    };
    const int keycount = 1;
    int i = 0;
    for (i = 0; i < keycount; ++i)
    {
        success = nfc.mifareclassic_AuthenticateBlock(uid, 4, block, 1, keyuniversal[i]);
        if (success)
            return true;
    }
    return false;
}
void AppPN532::loop()
{
    uint8_t success;                       // Flag to check if there was an error with the PN532
    uint8_t uid[] = {0, 0, 0, 0, 0, 0, 0}; // Buffer to store the returned UID
    uint8_t uidLength;                     // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
    uint8_t currentblock;                  // Counter to keep track of which block we're on
    bool authenticated = false;            // Flag to indicate if the sector is authenticated

    AppBase::loop();
    if (unready)
        appManager.goBack();
    if (writecardreq)
    {
        writecardreq = false;
        lv_toast("请放卡");
        success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 3000);
        if (success)
        {
            if (uidLength == 4)
            {
                char uid_str[30];
                sprintf(uid_str, "/%02x%02x%02x%02x.dump", uid[0], uid[1], uid[2], uid[3]);
                if (SPIFFS.exists(uid_str) == false)
                {
                    lv_toast("没有记录");
                    return;
                }
                File f = SPIFFS.open(uid_str, "r");
                if (!f)
                {
                    lv_toast("无法打开文件");
                    return;
                }
                int size = f.readBytes((char *)rfid_data, (CARD_END - CARD_START + 1) * 16);
                f.close();
                if (size != (CARD_END - CARD_START + 1) * 16)
                {
                    Serial.println(size);
                    msgbox("严重错误", "dump文件损坏");
                    return;
                }
                for (currentblock = CARD_START; currentblock <= CARD_END; currentblock++)
                {
                    // Check if this is a new block so that we can reauthenticate
                    if (nfc.mifareclassic_IsFirstBlock(currentblock))
                        authenticated = false;

                    // If the sector hasn't been authenticated, do so first
                    if (!authenticated)
                    {
                        success = rfidAuth(uid, currentblock);
                        if (success)
                        {
                            authenticated = true;
                        }
                        else
                        {
                            Serial.println("Authentication error");
                        }
                    }
                    // If we're still not authenticated just skip the block
                    if (!authenticated)
                    {
                        msgbox("错误", "无法认证，请确认卡片是否正确");
                        return;
                    }
                    else
                    {
                        // Authenticated ... we should be able to read the block now
                        // Dump the rfid_data into the 'rfid_data' array
                        success = nfc.mifareclassic_WriteDataBlock(currentblock, rfid_data[currentblock - CARD_START]);
                        if (success == false)
                        {
                            msgbox("错误", "无法读取，请确认卡片是否正确");
                            return;
                        }
                    }
                }
                msgbox("完成", "操作完成，请取走卡片");
                return;
            }
        }
        lv_toast("找不到目标");
    }
    success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 1000);
    if (success)
    {
        Serial.printf("Found Card:%02x%02x%02x%02x[%d]\n", uid[0], uid[1], uid[2], uid[3], uidLength);
        if (uidLength == 4)
        {
            // Now we try to go through all 16 sectors (each having 4 blocks)
            // authenticating each sector, and then dumping the blocks
            for (currentblock = CARD_START; currentblock <= CARD_END; currentblock++)
            {
                Serial.printf("CurrentBlock:%d .", currentblock);
                // Check if this is a new block so that we can reauthenticate
                if (nfc.mifareclassic_IsFirstBlock(currentblock))
                    authenticated = false;

                // If the sector hasn't been authenticated, do so first
                if (!authenticated)
                {
                    success = rfidAuth(uid, currentblock);
                    if (success)
                    {
                        authenticated = true;
                    }
                    else
                    {
                        Serial.println("Authentication error");
                    }
                }
                // If we're still not authenticated just skip the block
                if (!authenticated)
                {
                    msgbox("错误", "无法认证，请确认卡片是否正确");
                    return;
                }
                else
                {
                    // Authenticated ... we should be able to read the block now
                    // Dump the rfid_data into the 'rfid_data' array
                    Serial.print(".");
                    success = nfc.mifareclassic_ReadDataBlock(currentblock, rfid_data[currentblock - CARD_START]);
                    Serial.printf(".OK!\n");
                    if (success == false)
                    {
                        msgbox("错误", "无法读取，请确认卡片是否正确");
                        return;
                    }
                }
            }
            // 读取成功，显示信息
            // 写入文件
            if (msgbox_yn("读取完成，是否保存?"))
            {
                char uid_str[30];
                sprintf(uid_str, "/%02x%02x%02x%02x.dump", uid[0], uid[1], uid[2], uid[3]);
                if (SPIFFS.exists(uid_str))
                {
                    if (msgbox_yn("是否覆盖记录？") == false)
                    {
                        return;
                    }
                }
                File f = SPIFFS.open(uid_str, "w");
                if (!f)
                {
                    lv_toast("无法打开文件");
                    return;
                }
                f.write((uint8_t *)rfid_data, (CARD_END - CARD_START + 1) * 16);
                f.close();
                lv_toast("保存成功");
            }
        }
        else
        {
            lv_toast("这卡有点东西 (UID位数不对)");
        }
    }
}

void AppPN532::destruct()
{
    hal.DoNotSleep = false;
    Wire1.end();
    Wire1.begin(I2C1_SDA, I2C1_SCL, 400000);
}
