#include "A_Config.h"
#include <BleKeyboard.h>
static BleKeyboard bleKeyboard;
class AppKeyboard : public AppBase
{
private:
    /* data */
public:
    AppKeyboard()
    {
        name = "keyboard";
        title = "键盘";
        description = "一个只有空格键的蓝牙键盘，在状态变化时发送空格";
        image = "\xEF\x84\x9C";
    }
    void setup();
    void loop();
    void destruct();
};
lv_obj_t *btn_kb;
static void lv_event_kb(lv_event_t *e)
{
    switch (e->code)
    {
    case LV_EVENT_PRESSED:
    case LV_EVENT_RELEASED:
        bleKeyboard.write(' ');
        break;
    }
}
void AppKeyboard::setup()
{
    LOCKLV();
    btn_kb = lv_btn_create(scr);
    lv_obj_set_size(btn_kb, lv_pct(90), lv_pct(90));
    lv_obj_center(btn_kb);
    lv_obj_add_event_cb(btn_kb, lv_event_kb, LV_EVENT_ALL, NULL);
    UNLOCKLV();
    bleKeyboard.begin();
    hal.DoNotSleep = true;
}

void AppKeyboard::loop()
{
    AppBase::loop();
}

void AppKeyboard::destruct()
{
    bleKeyboard.end();
    btStop();
    hal.DoNotSleep = false;
}

static AppKeyboard app;
