#include "A_Config.h"

static lv_obj_t *bg;
static lv_obj_t *lblTime;
static lv_obj_t *btn;

static RTC_DATA_ATTR uint32_t start_timestamp = 0;
static RTC_DATA_ATTR uint32_t store_timestamp = 0;
static RTC_DATA_ATTR uint8_t running = false;
static bool ignore_click = false;
static void stopwatch_start()
{
    DateTime dt(hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
    start_timestamp = dt.unixtime();
    running = true;
}

static void stopwatch_stop()
{
    DateTime dt(hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
    uint32_t now_timestamp = dt.unixtime();
    running = false;
    store_timestamp += (now_timestamp - start_timestamp);
}

static void stopwatch_clear()
{
    running = false;
    start_timestamp = 0;
    store_timestamp = 0;
}
static void updateState()
{
    if (start_timestamp != 0)
    {
        if (running)
        {
            lv_obj_add_state(bg, LV_STATE_USER_1);
            lv_obj_clear_state(bg, LV_STATE_USER_2);
        }
        else
        {
            lv_obj_add_state(bg, LV_STATE_USER_2);
            lv_obj_clear_state(bg, LV_STATE_USER_1);
        }
    }
    else
    {
        lv_obj_clear_state(bg, LV_STATE_USER_1);
        lv_obj_clear_state(bg, LV_STATE_USER_2);
    }
}
static void event_btn_stopwatch(lv_event_t *e)
{
    auto code = lv_event_get_code(e);
    if (code == LV_EVENT_LONG_PRESSED)
    {
        stopwatch_clear();
        lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_PLAY);
        ignore_click = true;
        updateState();
    }
    else
    {
        if (ignore_click)
        {
            ignore_click = false;
            return;
        }
        if (running)
        {
            stopwatch_stop();
            lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_PLAY);
        }
        else
        {
            stopwatch_start();
            lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_PAUSE);
        }
        updateState();
    }
}

void tile_clock_stopwatch_loop()
{
    if (start_timestamp != 0)
    {
        if (running)
        {
            DateTime dt(hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
            uint32_t now_timestamp = dt.unixtime();

            uint32_t delta = now_timestamp - start_timestamp + store_timestamp;
            lv_label_set_text_fmt(lblTime, "%02d:%02d:%02d", delta / 3600, (delta % 3600) / 60, delta % 60);
        }
        else
        {
            lv_label_set_text_fmt(lblTime, "%02d:%02d:%02d", store_timestamp / 3600, (store_timestamp % 3600) / 60, store_timestamp % 60);
        }
    }
    else
    {
        lv_label_set_text(lblTime, "00:00:00");
    }
    updateState();
}

void tile_clock_stopwatch_create(lv_obj_t *tile)
{
    bg = lv_obj_create(tile);
    lv_obj_set_size(bg, 132, 56);
    lv_obj_set_pos(bg, 184, 180);
    lv_obj_pop_up(bg, 50, 500, 600);
    lv_obj_clear_flag(bg, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_pad_all(bg, 6, 0);

    lblTime = lv_label_create(bg);
    lv_obj_set_style_text_font(lblTime, &lv_font_chinese_16, 0);
    lv_obj_set_size(lblTime, lv_pct(70), LV_SIZE_CONTENT);
    lv_label_set_text(lblTime, "00:00:00");
    lv_obj_set_style_text_align(lblTime, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(lblTime, LV_ALIGN_LEFT_MID, 0, 0);

    btn = button(bg, LV_SYMBOL_PLAY, 0, 0, event_btn_stopwatch);
    lv_obj_align(btn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_add_event_cb(btn, event_btn_stopwatch, LV_EVENT_LONG_PRESSED, NULL);

    static const lv_style_prop_t props[] = {LV_STYLE_BG_COLOR, (lv_style_prop_t)0};
    static lv_style_transition_dsc_t trans_cleared;
    //没有运行过
    lv_style_transition_dsc_init(&trans_cleared, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_cleared;
    lv_style_init(&style_cleared);
    lv_style_set_bg_color(&style_cleared, lv_palette_lighten(LV_PALETTE_CYAN, 2));
    lv_style_set_transition(&style_cleared, &trans_cleared);

    //正在运行
    static lv_style_transition_dsc_t trans_running;
    lv_style_transition_dsc_init(&trans_running, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_running;
    lv_style_init(&style_running);
    lv_style_set_bg_color(&style_running, lv_palette_lighten(LV_PALETTE_GREEN, 3));
    lv_style_set_transition(&style_running, &trans_running);

    static lv_style_transition_dsc_t trans_paused;
    //已停止
    lv_style_transition_dsc_init(&trans_paused, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_paused;
    lv_style_init(&style_paused);
    lv_style_set_bg_color(&style_paused, lv_palette_lighten(LV_PALETTE_YELLOW, 2));
    lv_style_set_transition(&style_paused, &trans_cleared);

    lv_obj_add_style(bg, &style_cleared, 0);
    lv_obj_add_style(bg, &style_running, LV_STATE_USER_1);
    lv_obj_add_style(bg, &style_paused, LV_STATE_USER_2);
}