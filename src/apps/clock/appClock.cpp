#include "A_Config.h"
class AppClock : public AppBase
{
private:
    /* data */
public:
    AppClock()
    {
        name = "clock";
        title = "时钟";
        description = "时钟界面，启动后默认进入此界面";
        image = "\xEF\x80\x97";
    }
    void setup();
    void loop();
    void destruct();
};

static lv_obj_t *tile_clock;

extern void tile_clock_build(lv_obj_t *tile);
extern void tile_clock_loop();

static void getQQCount()
{
    if (schoolWiFi.client.connected() == false)
        if (schoolWiFi.connectTCP() == false)
        {
            lv_toast("无法连接到服务器");
            return;
        }
    schoolWiFi.client.write("{\"method\":\"count\",\"payload\":\"\"}");
    lv_toast(schoolWiFi.client.readStringUntil('\n').c_str());
}

extern uint8_t weather_last_sec;
void wf_clock_anim_set(lv_obj_t *label[2], uint16_t y, uint16_t delaytime)
{
    lv_obj_t *swap;
    lv_anim_t a;
    lv_anim_init(&a);
    lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t)lv_obj_set_y);
    lv_anim_set_path_cb(&a, lv_anim_path_ease_in);
    lv_anim_set_time(&a, 300);
    lv_anim_set_delay(&a, delaytime);
    lv_anim_set_var(&a, label[1]);
    lv_anim_set_values(&a, y - 32, y);
    lv_anim_start(&a);
    lv_obj_fade_in(label[1], 200, delaytime + 100);

    lv_anim_set_path_cb(&a, lv_anim_path_ease_in);
    lv_anim_set_var(&a, label[0]);
    lv_anim_set_values(&a, y, y + 32);
    lv_anim_start(&a);
    lv_obj_fade_out(label[0], 200, delaytime + 200);

    swap = label[0];
    label[0] = label[1];
    label[1] = swap;
}

void btn_bool_event(lv_event_t *e)
{
    bool *b = (bool *)lv_event_get_user_data(e);
    if (b)
        *b = true;
}

void AppClock::setup()
{
    LOCKLV();
    tile_clock_build(scr);
    UNLOCKLV();
}

void AppClock::loop()
{
    DateTime dt(hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
#ifdef CMD_PASSWORD
    int lastpass = settings.getInt("last_password", 0);
    if (abs((int)(dt.unixtime() / 86400) - lastpass) > 5)
    {
        while (1)
        {
            if (msgbox_passwd() == CMD_PASSWORD)
                break;
            vTaskDelay(5000);
        }
        settings.setInt("last_password", dt.unixtime() / 86400);
        settings.writeSettings(CONFIG_FILE_NAME);
        return;
    }
#endif
    if (hal.axpShortPress)
    {
        hal.canDeepSleep = false;
        hal.axpShortPress = false;
        menu_create();
        menu_add(LV_SYMBOL_LIST " App");
        menu_add(LV_SYMBOL_DOWNLOAD " 更新天气");
        menu_add(LV_SYMBOL_SAVE " 保存录音");
        menu_add(LV_SYMBOL_ENVELOPE " 截图打印");
        menu_add(LV_SYMBOL_CUT " 截图");
        menu_add(LV_SYMBOL_RIGHT " CMD");
        menu_add(LV_SYMBOL_DOWNLOAD " 连接服务器");
        int16_t choose = menu_show();
        switch (choose)
        {
        case 1:
            appManager.showAppList();
            return;
            break;
        case 2:
        {
            if (WiFiMgr.autoConnect())
            {
                weather.refresh();
                WiFi.disconnect(true);
                weather_last_sec = 99; // 立即更新天气信息
            }
            break;
        }
        case 3:
        {
            ESPNow.start();
            bool suc = ESPNow.sendCmd(CMD_RECORD, "");
            ESPNow.end();
            if (suc)
                msgbox("完成", "指令已发送并已收到应答", 1000);
            break;
        }
        case 4:
        {
            char param[50];
            bool suc = false;
            menu_create();
            menu_add("Floyd-Steinberg");
            menu_add("黑白");
            menu_add("反色黑白");
            menu_add("希沃白板模式(TODO)");
            switch (menu_show())
            {
            case 0:
                return;
                break;
            case 1:
                strcpy(param, "print.jpg dither");
                break;
            case 2:
                strcpy(param, "print.jpg bw");
                break;
            case 3:
                strcpy(param, "print.jpg bw_inv");
                break;
            case 4:
                strcpy(param, "print.jpg seewo");
                break;
            default:
                return;
                break;
            }
            hal.DoNotSleep = true;
            ESPNow.start();
            auto msg = full_screen_msgbox_create(BIG_SYMBOL_DOWNLOAD, "远程打印", "正在截图并打印，请稍候");
            vTaskDelay(500);
            if (ESPNow.sendCmd(CMD_SCREENSHOT, "print.jpg"))
            {
                vTaskDelay(300);
                if (ESPNow.sendCmd(CMD_PROCIMG, param))
                {
                    vTaskDelay(500);
                    if (ESPNow.sendCmd(CMD_LOADFILE, "out.bin"))
                    {
                        vTaskDelay(300);
                        if (ESPNow.recvFile(printer.buffer, ESPNOW_MASTER_DEVICEID))
                        {
                            uint16_t count = 0;
                            count = msgbox_number("请输入打印份数", 5, 1, 1);
                            xSemaphoreTake(hal.wiremutex, portMAX_DELAY);
                            size_t size = ESPNow.getLastFileSize();
                            if (printer.testConnection())
                            {
                                for (uint16_t i = 0; i < count; ++i)
                                {
                                    printer.setMaxY(size / 48);
                                    printer.sendBuffer();
                                    printer.gomm(5);
                                    printer.endPrint();
                                }
                                suc = true;
                            }
                            xSemaphoreGive(hal.wiremutex);
                        }
                    }
                }
            }
            else
            {
                vTaskDelay(600);
            }
            full_screen_msgbox_del(msg);
            ESPNow.end();
            if (suc)
            {
                full_screen_msgbox(BIG_SYMBOL_CHECK, "完成", "打印完成", FULL_SCREEN_BG_CHECK, 4000);
            }
            hal.DoNotSleep = false;
        }
        break;
        case 5:
        {
            char filename[40];
            sprintf(filename, "ss/%02d-%02d-%02d_%02d%02d%02d.jpg", hal.rtc.year, hal.rtc.month, hal.rtc.day, hal.rtc.hour, hal.rtc.minute, hal.rtc.sec);
            ESPNow.start();
            bool suc = ESPNow.sendCmd(CMD_SCREENSHOT, filename);
            ESPNow.end();
            if (suc)
                msgbox("完成", filename, 1000);
        }
        break;
        case 6:
        {
            String cmd = msgbox_string("~#", false, false, false);
            if (cmd.length() > 0)
            {
                int param = cmd.indexOf(" ");
                if (param > 0)
                {
                    appManager.parameter = cmd.substring(param + 1, cmd.length());
                }
                int id = appManager.getIDByName(cmd.substring(0, param).c_str());
                if (id > 0)
                {
                    appManager.gotoApp(id);
                    return;
                }
            }
            break;
        }
        case 7:
        {
            WiFiMgr.autoConnect();
            getQQCount();
            WiFi.disconnect(true);
            break;
        }
        default:
            break;
        }
    }
    tile_clock_loop();
}

void AppClock::destruct()
{
}

static AppClock app;