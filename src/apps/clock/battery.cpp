#include "A_Config.h"

static lv_obj_t *obj_battery;
static lv_obj_t *bar_battery;
static lv_obj_t *lbl_voltage;
static lv_obj_t *lbl_current;
static lv_obj_t *lbl_coulomb;

static lv_obj_t *lbl_voltage_VBus;
static lv_obj_t *lbl_current_VBus;
static lv_obj_t *lbl_voltage_ACIN;
static lv_obj_t *lbl_current_ACIN;

static bool ischarging = false;
static float voltage, current, columb;
static float voltage_VBus, current_VBus;
static float voltage_ACIN, current_ACIN;

static void event_obj_battery_press(lv_event_t *e)
{
    lv_obj_fade_out(lbl_voltage, 300, 0);
    lv_obj_fade_out(lbl_current, 300, 100);
    lv_obj_fade_out(lbl_coulomb, 300, 200);

    lv_obj_fade_in(lbl_voltage_VBus, 300, 100);
    lv_obj_fade_in(lbl_current_VBus, 300, 200);
    lv_obj_fade_in(lbl_voltage_ACIN, 300, 300);
    lv_obj_fade_in(lbl_current_ACIN, 300, 400);
}

static void event_obj_battery_press_lost(lv_event_t *e)
{
    lv_obj_fade_out(lbl_voltage_VBus, 300, 0);
    lv_obj_fade_out(lbl_current_VBus, 300, 100);
    lv_obj_fade_out(lbl_voltage_ACIN, 300, 200);
    lv_obj_fade_out(lbl_current_ACIN, 300, 300);

    lv_obj_fade_in(lbl_voltage, 300, 100);
    lv_obj_fade_in(lbl_current, 300, 200);
    lv_obj_fade_in(lbl_coulomb, 300, 300);
}

static uint16_t calculateBattery(float vol)
{
    if (vol > BATTERY_100_VOLTAGE)
        return 100;
    if (vol < BATTERY_1_VOLTAGE)
        return 1;
    return ((vol - BATTERY_1_VOLTAGE) / (BATTERY_100_VOLTAGE - BATTERY_1_VOLTAGE) * 100);
}

void tile_clock_battery_loop()
{
    char buftmp[40];
    //更新电池信息
    xSemaphoreTake(hal.wiremutex, portMAX_DELAY);
    Wire.flush();
    ischarging = hal.axp.isCharging();
    if (ischarging)
        current = hal.axp.getBattChargeCurrent();
    else
        current = hal.axp.getBattDischargeCurrent();
    voltage = hal.axp.getBattVoltage();
    columb = hal.axp.getCoulombData();
    voltage_ACIN = hal.axp.getAcinVoltage();
    current_ACIN = hal.axp.getAcinCurrent();
    voltage_VBus = hal.axp.getVbusVoltage();
    current_VBus = hal.axp.getVbusCurrent();
    xSemaphoreGive(hal.wiremutex);
    sprintf(buftmp, "VBat=%.2f V", voltage / 1000);
    lv_label_set_text(lbl_voltage, buftmp);

    sprintf(buftmp, "IBat= %.1f mA", current);
    lv_label_set_text(lbl_current, buftmp);

    if (settings.getBool("settings.encoulomb", false) == true)
    {
        sprintf(buftmp, "Q= %.2f mAh", columb);
        lv_label_set_text(lbl_coulomb, buftmp);
    }
    else
    {
        lv_label_set_text(lbl_coulomb, "库仑计已关闭");
    }

    if (voltage_VBus > 100)
    {
        sprintf(buftmp, "V1= %.2f V", voltage_VBus / 1000);
        lv_label_set_text(lbl_voltage_VBus, buftmp);
    }
    else
    {
        lv_label_set_text(lbl_voltage_VBus, "USB1 未接入");
    }
    sprintf(buftmp, "I1= %.1f mA", current_VBus);
    lv_label_set_text(lbl_current_VBus, buftmp);

    if (voltage_ACIN > 100)
    {
        sprintf(buftmp, "V2= %.2f V", voltage_ACIN / 1000);
        lv_label_set_text(lbl_voltage_ACIN, buftmp);
    }
    else
    {
        lv_label_set_text(lbl_voltage_ACIN, "USB2 未接入");
    }
    sprintf(buftmp, "I2= %.1f mA", current_ACIN);
    lv_label_set_text(lbl_current_ACIN, buftmp);

    lv_bar_set_value(bar_battery, calculateBattery(voltage), LV_ANIM_ON);
    if (ischarging)
    {
        lv_obj_clear_state(obj_battery, LV_STATE_USER_1);
    }
    else
    {
        lv_obj_add_state(obj_battery, LV_STATE_USER_1);
    }
}

#define PAGE1_OFFSET 9
#define PAGE2_OFFSET 3
void tile_clock_battery_create(lv_obj_t *tile)
{
    obj_battery = lv_obj_create(tile);
    lv_obj_set_size(obj_battery, 132, 84);
    lv_obj_set_pos(obj_battery, 184, 92);
    lv_obj_pop_up(obj_battery, 50, 500, 300);
    lv_obj_clear_flag(obj_battery, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_pad_all(obj_battery, 4, 0);
    lv_obj_add_event_cb(obj_battery, event_obj_battery_press, LV_EVENT_PRESSED, NULL);
    lv_obj_add_event_cb(obj_battery, event_obj_battery_press_lost, LV_EVENT_RELEASED, NULL);

    bar_battery = lv_bar_create(obj_battery);
    lv_obj_set_size(bar_battery, 10, 76);
    lv_obj_align(bar_battery, LV_ALIGN_LEFT_MID, 0, 0);
    lv_bar_set_value(bar_battery, 100, LV_ANIM_OFF);
    lv_bar_set_range(bar_battery, 0, 100);

    lbl_voltage = label(obj_battery, "---", 12, 0 + PAGE1_OFFSET);
    lbl_current = label(obj_battery, "---", 12, 16 + PAGE1_OFFSET);
    lbl_coulomb = label(obj_battery, "---", 12, 32 + PAGE1_OFFSET);

    lbl_voltage_VBus = label(obj_battery, "---", 12, 0 + PAGE2_OFFSET);
    lbl_current_VBus = label(obj_battery, "---", 12, 16 + PAGE2_OFFSET);
    lbl_voltage_ACIN = label(obj_battery, "---", 12, 32 + PAGE2_OFFSET);
    lbl_current_ACIN = label(obj_battery, "---", 12, 48 + PAGE2_OFFSET);

    lv_obj_set_style_opa(lbl_voltage_VBus, 0, 0);
    lv_obj_set_style_opa(lbl_current_VBus, 0, 0);
    lv_obj_set_style_opa(lbl_voltage_ACIN, 0, 0);
    lv_obj_set_style_opa(lbl_current_ACIN, 0, 0);

    static const lv_style_prop_t props[] = {LV_STYLE_BG_COLOR, (lv_style_prop_t)0};
    static lv_style_transition_dsc_t trans_discharging;
    //正放电
    lv_style_transition_dsc_init(&trans_discharging, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_discharging;
    lv_style_init(&style_discharging);
    lv_style_set_bg_color(&style_discharging, lv_palette_lighten(LV_PALETTE_CYAN, 4));
    lv_style_set_transition(&style_discharging, &trans_discharging);

    //正充电
    static lv_style_transition_dsc_t trans_charging;
    lv_style_transition_dsc_init(&trans_charging, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_charging;
    lv_style_init(&style_charging);
    lv_style_set_bg_color(&style_charging, lv_palette_lighten(LV_PALETTE_GREEN, 4));
    lv_style_set_transition(&style_charging, &trans_charging);

    lv_obj_add_style(obj_battery, &style_discharging, LV_STATE_USER_1);
    lv_obj_add_style(obj_battery, &style_charging, 0);
    tile_clock_battery_loop();
}