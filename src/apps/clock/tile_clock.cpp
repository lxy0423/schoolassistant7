#include "A_Config.h"

static uint8_t last_sec = 0;

extern void btn_bool_event(lv_event_t *e);
void tile_clock_timetable_create(lv_obj_t *tile);
void tile_clock_timetable_loop();
void tile_clock_timetable_loop1();
void tile_clock_clock_loop();
void tile_clock_clock_create(lv_obj_t *tile);
void tile_clock_remaining_create(lv_obj_t *tile);
void tile_clock_battery_create(lv_obj_t *tile);
void tile_clock_battery_loop();
void tile_clock_stopwatch_create(lv_obj_t *tile);
void tile_clock_stopwatch_loop();

void tile_clock_loop()
{
    tile_clock_timetable_loop1();
    if (last_sec != hal.rtc.sec)
    {
        last_sec = hal.rtc.sec;
        tile_clock_clock_loop();
        tile_clock_timetable_loop();
        tile_clock_battery_loop();
        tile_clock_stopwatch_loop();
    }
    vTaskDelay(200);
}

void tile_clock_build(lv_obj_t *tile)
{
    tile_clock_clock_create(tile);

    tile_clock_timetable_create(tile);

    tile_clock_remaining_create(tile);

    tile_clock_battery_create(tile);
    
    tile_clock_stopwatch_create(tile);
}