#include "A_Config.h"

static lv_obj_t *lblTitle;
static lv_obj_t *lblTime[4][2];
static lv_obj_t *bg;

static RTC_DATA_ATTR uint8_t digits[4];
static uint8_t digits_now[4];
static uint16_t label_y[4];

extern void wf_clock_anim_set(lv_obj_t *label[2], uint16_t y, uint16_t delaytime);

void setRemaining(uint32_t time, bool isClassOver)
{
    if (time > 99 * 60 + 59)
        time = 99 * 60 + 59;
    uint8_t minute = time / 60;
    uint8_t second = time % 60;
    digits_now[0] = minute / 10;
    digits_now[1] = minute % 10;
    digits_now[2] = second / 10;
    digits_now[3] = second % 10;
    for (uint8_t i = 0; i < 4; ++i)
    {
        if (digits[i] != digits_now[i])
        {
            digits[i] = digits_now[i];
            lv_label_set_text_fmt(lblTime[i][1], "%d", digits[i] % 10);
            wf_clock_anim_set(lblTime[i], label_y[i], 0);
        }
    }
    if (isClassOver)
    {
        if (time == 99 * 60 + 59)
            lv_label_set_text(lblTitle, "离上课还有很久");
        else
            lv_label_set_text(lblTitle, "距离上课还有：");
        lv_obj_clear_state(bg, LV_STATE_USER_1);
    }
    else
    {
        lv_label_set_text(lblTitle, "距离下课还有：");
        lv_obj_add_state(bg, LV_STATE_USER_1);
    }
}
#define POS_ADJUST 3
#define TIME_DIGIT_Y 20
void tile_clock_remaining_create(lv_obj_t *tile)
{
    bg = lv_obj_create(tile);
    lv_obj_set_size(bg, 132, 84);
    lv_obj_set_pos(bg, 184, 4);
    lv_obj_pop_up(bg, 50, 500, 200);
    lv_obj_clear_flag(bg, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_pad_all(bg, 6, 0);

    static const lv_style_prop_t props[] = {LV_STYLE_BG_COLOR, (lv_style_prop_t)0};
    static lv_style_transition_dsc_t trans_class;
    //上课时间段
    lv_style_transition_dsc_init(&trans_class, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_class;
    lv_style_init(&style_class);
    lv_style_set_bg_color(&style_class, lv_palette_main(LV_PALETTE_ORANGE));
    lv_style_set_transition(&style_class, &trans_class);

    //下课时间段
    static lv_style_transition_dsc_t trans_classover;
    lv_style_transition_dsc_init(&trans_classover, props, lv_anim_path_linear, 300, 0, NULL);
    static lv_style_t style_classover;
    lv_style_init(&style_classover);
    lv_style_set_bg_color(&style_classover, lv_palette_main(LV_PALETTE_GREEN));
    lv_style_set_transition(&style_classover, &trans_classover);

    lv_obj_add_style(bg, &style_class, LV_STATE_USER_1);
    lv_obj_add_style(bg, &style_classover, 0);

    for (uint8_t i = 0; i < 4; ++i)
    {
        lblTime[i][0] = lv_label_create(bg);
        lv_obj_set_align(lblTime[i][0], LV_ALIGN_TOP_LEFT);
        lv_obj_set_style_text_font(lblTime[i][0], &num_64px, 0);
        lv_label_set_text(lblTime[i][0], "9");
        lblTime[i][1] = lv_label_create(bg);
        lv_obj_set_align(lblTime[i][1], LV_ALIGN_TOP_LEFT);
        lv_obj_set_style_text_font(lblTime[i][1], &num_64px, 0);
        lv_label_set_text(lblTime[i][1], "");
        lv_obj_set_x(lblTime[i][0], 31 * i - POS_ADJUST);
        lv_obj_set_y(lblTime[i][0], TIME_DIGIT_Y);
        lv_obj_set_x(lblTime[i][1], 31 * i - POS_ADJUST);
        lv_obj_set_y(lblTime[i][1], label_y[i] = TIME_DIGIT_Y); //暂存y,之后动画要用到
        lv_obj_set_style_text_color(lblTime[i][0], lv_color_white(), 0);
        lv_obj_set_style_text_color(lblTime[i][1], lv_color_white(), 0);
    }
    for (uint8_t i = 0; i < 4; ++i)
    {
        lv_label_set_text_fmt(lblTime[i][0], "%d", digits[i] % 10);
    }
    lblTitle = lv_label_create(bg);
    lv_obj_set_style_text_font(lblTitle, &lv_font_chinese_16, 0);
    lv_obj_align(lblTitle, LV_ALIGN_TOP_MID, 0, 0);
    lv_obj_set_style_text_color(lblTitle, lv_color_white(), 0);
    lv_label_set_text(lblTitle, "离上课还有很久");
}
