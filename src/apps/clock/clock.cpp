#include "A_Config.h"

static alarm_t *curr_class = NULL;
static alarm_t *next_class = NULL;
static alarm_t *breakflag = NULL; // 用于是否下课

static lv_obj_t *bg;
static lv_obj_t *lblDate;
static lv_obj_t *lblTime[6][2];

static RTC_DATA_ATTR uint8_t digits[6];
static uint8_t digits_now[6];
static uint16_t label_y[6];

static uint32_t remaining;
extern void setRemaining(uint32_t time, bool isClassOver);

extern void wf_clock_anim_set(lv_obj_t *label[2], uint16_t y, uint16_t delaytime);

void tile_clock_clock_loop()
{
    alarm_check();
    // 更新时间、日期、闹钟、倒计时
    // uint8_t start_i = 99;
    uint8_t week;
    uint8_t sec;
    uint8_t minute;
    uint8_t hour;
    uint16_t p;
    bool isclassover = false;
    hour = hal.rtc.hour;
    digits_now[0] = hour / 10;
    digits_now[1] = hour % 10;
    minute = hal.rtc.minute;
    sec = hal.rtc.sec;
    digits_now[2] = minute / 10;
    digits_now[3] = minute % 10;
    digits_now[4] = sec / 10;
    digits_now[5] = sec % 10;
    for (uint8_t i = 0; i < 6; ++i)
    {
        if (digits[i] != digits_now[i])
        {
            // if (start_i == 99)
            //     start_i = i;
            digits[i] = digits_now[i];
            lv_label_set_text_fmt(lblTime[i][1], "%d", digits[i] % 10);
            wf_clock_anim_set(lblTime[i], label_y[i], 0); // (i - start_i) * 100);
        }
    }
    lv_label_set_text_fmt(lblDate, "%02d/%02d/%02d  %s", hal.rtc.year,
                          hal.rtc.month, hal.rtc.day,
                          week_name[week = hal.rtc.weekday]);
    p = hour * 60 + minute;
    if (curr_class != class_get_curr(week, p) || breakflag != class_get_next(week, p))
    {
        // 更新闹钟信息
        breakflag = class_get_next(week, p);
        curr_class = class_get_curr(week, p);
        next_class = class_get_next_no_curr(week, p);
        alarm_update();
        if (next_class == NULL)
        {
            next_class = class_get_next_no_curr(0, 0);
        }
    }
    if (next_class != NULL)
    {
        if (next_class->week != week)
        {
            hal.canDeepSleepFromAlarm = true;
        }
    }
    else
    {
        hal.canDeepSleepFromAlarm = true;
    }
    uint32_t maxval = 0xffffffff; // 总共还有多少分钟，并在后面转换为秒数，实际还有多少秒看remaining
    uint8_t dweek;                // 用于存储不同天闹钟相差天数
    if (curr_class == NULL)
    {
        if (next_class == NULL)
        {
            // 没有添加任何课程
            remaining = 0xffffffff;
            isclassover = true;
        }
        else
        {
            // 从0:00到现在还没有上过课，计算距离上课剩余时间
            // 这节课是否在下一周？或者在同一天，但是已经过了
            dweek = (next_class->week < week || (p >= next_class->time_start && next_class->week == week))
                        ? next_class->week + 7
                        : next_class->week;
            dweek -= week;
            maxval = remaining = dweek * 24 * 60 + next_class->time_start - p;
            isclassover = true;
        }
    }
    else if (next_class->week != week && p >= curr_class->time_end)
    {
        // 类似上面的情况，但是今天有课，且已经上完
        dweek = next_class->week <= week ? next_class->week + 7 : next_class->week;
        dweek -= week;
        maxval = remaining = dweek * 24 * 60 + next_class->time_start - p;
        isclassover = true;
    }
    else if (next_class->time_start <= p && curr_class->time_end <= p)
    {
        // 当前课程已结束，但是下节课在下周的同一天
        maxval = remaining = 7 * 24 * 60 + next_class->time_start - p;
        isclassover = true;
    }
    else if (curr_class->time_end <= p)
    {
        // 当前课程已结束，下一节课还未开始，且在同一天，如果闹钟没有冲突，此时一定有next_class->time_start > p
        remaining = next_class->time_start - p;
        maxval = next_class->time_start - curr_class->time_end;
        isclassover = true;
    }
    else
    {
        // 当前课程未结束
        remaining = curr_class->time_end - p;
        maxval = curr_class->time_end - curr_class->time_start;
        isclassover = false;
    }
    if (remaining != 0xffffffff)
    {
        remaining *= 60;
        remaining -= sec;
        maxval *= 60;
    }
    setRemaining(remaining, isclassover);
}

#define TIME_DIGIT_Y 20
#define TIME_DIGIT_SMALL_Y (TIME_DIGIT_Y + 24)
// 创建时钟控件
void tile_clock_clock_create(lv_obj_t *tile)
{
    uint8_t week = hal.rtc.weekday;
    uint16_t p = hal.rtc.hour * 60 + hal.rtc.minute;
    breakflag = class_get_next(week, p);
    curr_class = class_get_curr(week, p);
    next_class = class_get_next_no_curr(week, p);
    if (next_class == NULL)
    {
        next_class = class_get_next_no_curr(0, 0);
    }
    //////////////////////////////////////////////////////////////////////////////////////
    bg = lv_obj_create(tile);
    lv_obj_set_size(bg, 176, 84);
    lv_obj_set_pos(bg, 4, 4);
    lv_obj_pop_up(bg, 50, 500);
    lv_obj_clear_flag(bg, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_pad_all(bg, 6, 0);

    lv_obj_set_style_bg_color(bg, lv_palette_main(LV_PALETTE_BLUE), 0);
    for (uint8_t i = 0; i < 4; ++i)
    {
        lblTime[i][0] = lv_label_create(bg);
        lv_obj_set_align(lblTime[i][0], LV_ALIGN_TOP_LEFT);
        lv_obj_set_style_text_font(lblTime[i][0], &num_64px, 0);
        lv_label_set_text(lblTime[i][0], "0");
        lblTime[i][1] = lv_label_create(bg);
        lv_obj_set_align(lblTime[i][1], LV_ALIGN_TOP_LEFT);
        lv_obj_set_style_text_font(lblTime[i][1], &num_64px, 0);
        lv_label_set_text(lblTime[i][1], "");
        lv_obj_set_x(lblTime[i][0], 32 * i);
        lv_obj_set_y(lblTime[i][0], TIME_DIGIT_Y);
        lv_obj_set_x(lblTime[i][1], 32 * i);
        lv_obj_set_y(lblTime[i][1], label_y[i] = TIME_DIGIT_Y); // 暂存y,之后动画要用到
        lv_obj_set_style_text_color(lblTime[i][0], lv_color_white(), 0);
        lv_obj_set_style_text_color(lblTime[i][1], lv_color_white(), 0);
    }
    for (uint8_t i = 4; i < 6; ++i)
    {
        lblTime[i][0] = lv_label_create(bg);
        lv_obj_set_align(lblTime[i][0], LV_ALIGN_TOP_LEFT);
        lv_obj_set_style_text_font(lblTime[i][0], &num_32px, 0);
        lv_label_set_text(lblTime[i][0], "0");
        lblTime[i][1] = lv_label_create(bg);
        lv_obj_set_align(lblTime[i][1], LV_ALIGN_TOP_LEFT);
        lv_obj_set_style_text_font(lblTime[i][1], &num_32px, 0);
        lv_label_set_text(lblTime[i][1], "");
        lv_obj_set_x(lblTime[i][0], 64 + 16 * i);
        lv_obj_set_y(lblTime[i][0], TIME_DIGIT_SMALL_Y);
        lv_obj_set_x(lblTime[i][1], 64 + 16 * i);
        lv_obj_set_y(lblTime[i][1], label_y[i] = TIME_DIGIT_SMALL_Y);
        lv_obj_set_style_text_color(lblTime[i][0], lv_color_white(), 0);
        lv_obj_set_style_text_color(lblTime[i][1], lv_color_white(), 0);
    }
    for (uint8_t i = 0; i < 6; ++i)
    {
        lv_label_set_text_fmt(lblTime[i][0], "%d", digits[i] % 10);
    }
    lblDate = lv_label_create(bg);
    lv_obj_set_style_text_font(lblDate, &lv_font_chinese_16, 0);
    lv_obj_align(lblDate, LV_ALIGN_TOP_MID, 0, 0);
    lv_obj_set_style_text_color(lblDate, lv_color_white(), 0);
}
