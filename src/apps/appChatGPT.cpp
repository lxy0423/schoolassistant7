#include "A_Config.h"
LV_IMG_DECLARE(chatgpt_logo);
static lv_obj_t *ta_chat;
static bool sendreq = false;
static bool clearreq = false;
#include <iostream>
#include <string>

String escape_unicode(const String &input)
{
    String output;
    for (auto c : input)
    {
        switch (c)
        {
        case '\"':
            output += "\\\"";
            break;
        case '\\':
            output += "\\\\";
            break;
        case '\b':
            output += "\\b";
            break;
        case '\f':
            output += "\\f";
            break;
        case '\n':
            output += "\\n";
            break;
        case '\r':
            output += "\\r";
            break;
        case '\t':
            output += "\\t";
            break;
        default:
            if (c >= 0x20 && c <= 0x7E)
            {
                output += c;
            }
            else
            {
                char buf[7];
                sprintf(buf, "\\u%04X", c);
                output += buf;
            }
        }
    }
    return output;
}

class AppChatGPT : public AppBase
{
private:
    /* data */
    bool flagback = false;
    String text_json_history = "";
    lv_obj_t *chk_multiple;
    lv_obj_t *btn_clear;
    lv_obj_t *btn_send;

public:
    AppChatGPT()
    {
        name = "chatgpt";
        title = "ChatGPT";
        description = "一个基于GPT-3.5的聊天机器人";
        image = &chatgpt_logo;
    }
    void setup();
    void loop();
    void destruct();
    void getReply(String text)
    {
        DynamicJsonDocument doc(8192);
        if (text.length() == 0)
            return;
        lv_toast("正在处理...");
        if (lv_obj_has_state(chk_multiple, LV_STATE_CHECKED) == false)
        {
            lv_textarea_set_text(ta_chat, "");
            text_json_history = "";
        }
        lv_textarea_set_cursor_pos(ta_chat, LV_TEXTAREA_CURSOR_LAST);
        lv_textarea_add_text(ta_chat, "\n>> ");
        lv_textarea_add_text(ta_chat, text.c_str());

        text_json_history += "{\"role\": \"user\", \"content\": \"" + escape_unicode(text) + "\"}";
        String datastr = "{\"model\": \"" CHAT_GPT_MODEL "\", \"temperature\": 0.5, \"messages\": [" + text_json_history + "]}";
        text_json_history += ",";
        Serial.println(datastr);
        schoolWiFi.https.begin(CHAT_GPT_API_URL "/v1/chat/completions");
        schoolWiFi.https.addHeader("Content-Type", "application/json");
        schoolWiFi.https.addHeader("Authorization", "Bearer " CHAT_GPT_API_KEY);
        int httpCode = schoolWiFi.https.POST(datastr);
        Serial.println(httpCode);
        if (httpCode > 0)
        {
            if (httpCode == HTTP_CODE_OK)
            {
                String payload = schoolWiFi.https.getString();
                Serial.println(payload);
                deserializeJson(doc, payload);
                String reply = doc["choices"][0]["message"]["content"].as<String>();
                lv_textarea_set_cursor_pos(ta_chat, LV_TEXTAREA_CURSOR_LAST);
                lv_textarea_add_text(ta_chat, "\n");
                lv_textarea_add_text(ta_chat, reply.c_str());
                text_json_history += doc["choices"][0]["message"].as<String>();
                text_json_history += ",";
                Serial.println(text_json_history);
                schoolWiFi.https.end();
                return;
            }
            lv_toast("失败");
        }
        else
        {
            lv_toast("网络错误");
        }
        schoolWiFi.https.end();
    }
};
static AppChatGPT app;

void AppChatGPT::setup()
{
    hal.DoNotSleep = true;
    lv_toast("连接WiFi...");
    if (WiFiMgr.autoConnect())
    {
        lv_toast("WiFi连接成功");
    }
    else
    {
        lv_toast("WiFi连接失败");
        flagback = true;
        return;
    }
    chk_multiple = lv_checkbox_create(scr);
    lv_checkbox_set_text(chk_multiple, "多轮对话");
    lv_obj_set_style_text_font(chk_multiple, &lv_font_chinese_16, 0);
    lv_obj_align(chk_multiple, LV_ALIGN_TOP_LEFT, 10, 13);

    btn_clear = button(scr, "清空", 180, 10, &clearreq);
    btn_send = button(scr, "发送", 250, 10, &sendreq);

    ta_chat = lv_textarea_create(scr);
    lv_obj_set_size(ta_chat, 300, 170);
    lv_obj_set_pos(ta_chat, 10, 60);
    lv_obj_set_style_text_font(ta_chat, &lv_font_chinese_16, 0);
    lv_textarea_set_text(ta_chat, "");
}

void AppChatGPT::loop()
{
    if (flagback)
    {
        flagback = false;
        appManager.goBack();
    }
    if (clearreq)
    {
        clearreq = false;
        lv_textarea_set_text(ta_chat, "");
        text_json_history = "";
    }
    if (sendreq)
    {
        sendreq = false;
        String text = msgbox_string("请输入要发送的内容", false, false, true);
        getReply(text);
    }
    AppBase::loop();
}

void AppChatGPT::destruct()
{
    hal.DoNotSleep = false;
    WiFi.disconnect(true);
}
