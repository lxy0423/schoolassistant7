#include "A_Config.h"

SchoolWiFi schoolWiFi;

bool SchoolWiFi::testConnection()
{
    for (uint8_t i = 0; i < 3; ++i)
    {
        if (client.connect(testHost, testPort))
        {
            client.stop();
            return true;
        }
        delay(500);
    }
    return false;
}
bool SchoolWiFi::connect()
{
    uint8_t tries = 0;
    bool donotsleepstatus = hal.DoNotSleep;
    hal.DoNotSleep = true;
    hal.axpShortPress = false;
    WiFi.mode(WIFI_STA);
    WiFi.begin(SCHOOL_WIFI_SSID, SCHOOL_WIFI_PASSWD);
    while (WiFi.status() != WL_CONNECTED)
    {
        vTaskDelay(500);
        ++tries;
        if (tries > 60 || WiFi.status() == WL_CONNECT_FAILED)
        {
            goto error;
        }
        if (hal.axpShortPress)
        {
            hal.axpShortPress = false;
            goto error;
        }
    }
    hal.DoNotSleep = donotsleepstatus;
    return true;
error:
    WiFi.mode(WIFI_OFF);
    hal.DoNotSleep = donotsleepstatus;
    return false;
}
bool SchoolWiFi::authenticate()
{
    String url = SCHOOL_WIFI_HOST;
    String macAddress;
    url += "?userip=";
    url += WiFi.localIP().toString();
    url += "&usermac=";
    macAddress = WiFi.macAddress();
    macAddress.replace(":", "");
    url += macAddress;
    http.begin(url);
    int httpCode = http.POST(SCHOOL_WIFI_POST_DATA);
    if (httpCode > 0)
    {
        if (httpCode == 302)
        {
            return true;
        }
    }
    return false;
}

bool SchoolWiFi::uploadFile(const String path, const char *remote_name)
{
    if (hal.SDMounted == false)
        return false;
    if (client.connected() == false)
        return false;
    int size;
    File f;
    f = SDCARD.open(path, "r");
    if (!f)
        return false;
    size = f.size();
    String pack = "{\"method\":\"sendfile\",\"payload\":\"" + String(remote_name) + "\",\"size\":" + String(size) + "}";
    schoolWiFi.client.write(pack.c_str());
    delay(600);
    schoolWiFi.client.write(f);
    f.close();
    schoolWiFi.client.readStringUntil('\n');
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////                                文件下载                                            ////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


static lv_obj_t *window_progress;
static lv_obj_t *bar_progress;
static String urlToDown;
static bool downloadSuccessful = false;
static File *f_download;
static SemaphoreHandle_t semaphoreDownload;

static void create_progress_bar()
{
    window_progress = lv_obj_create(lv_layer_top());
    lv_obj_set_size(window_progress, 240, 60);
    lv_obj_center(window_progress);
    bar_progress = lv_bar_create(window_progress);
    lv_obj_set_size(bar_progress, 200, 10);
    lv_obj_align(bar_progress, LV_ALIGN_BOTTOM_MID, 0, 0);
    lv_bar_set_value(bar_progress, 0, LV_ANIM_OFF);
    lv_obj_t *lbl_progress = lv_label_create(window_progress);
    lv_label_set_text(lbl_progress, "Downloading...");
    lv_obj_align(lbl_progress, LV_ALIGN_TOP_MID, 0, 0);
    lv_obj_pop_up(window_progress, 24, 300, 0);
}

static void del_progress_bar()
{
    lv_obj_fall_down(window_progress, 24, 250, 0);
    lv_obj_del_delayed(window_progress, 260);
}

static void task_music_downloader(void *)
{
    xSemaphoreTake(semaphoreDownload, portMAX_DELAY);
    HTTPClient http;
    http.begin(urlToDown);
    int httpCode = http.GET();
    if (httpCode == HTTP_CODE_OK)
    {
        // get length of document (is -1 when Server sends no Content-Length header)
        int len = http.getSize();
        if (len == -1)
            goto err;
        // create buffer for read
        uint8_t *buff;
        buff = (uint8_t *)ps_malloc(10240);
        // get tcp stream
        WiFiClient *stream = http.getStreamPtr();
        stream->setTimeout(5);
        // read all data from server
        size_t progLast = 0;
        size_t sizeSaved = 0;
        while (http.connected() && (sizeSaved != len))
        {
            // get available data size
            size_t size = stream->available();
            if (size)
            {
                int c = stream->readBytes(buff, len - sizeSaved > 10240 ? 10240 : len - sizeSaved);
                f_download->write(buff, c);
                sizeSaved += c;
            }
            else
            {
                delay(1);
                continue;
            }
            size_t progNow = sizeSaved * 100;
            progNow /= len;
            if (progLast != progNow)
            {
                progLast = progNow;
                lv_bar_set_value(bar_progress, progNow, LV_ANIM_ON);
                delay(10);
            }
        }
        free(buff);
    }
    else
        goto err;

    http.end();
    f_download->close();
    downloadSuccessful = true;
    xSemaphoreGive(semaphoreDownload);
    vTaskDelete(NULL);
    return;
err:
{
    String pathto = f_download->path();
    f_download->close();
    SDCARD.remove(pathto);
    http.end();
    downloadSuccessful = false;
    xSemaphoreGive(semaphoreDownload);
    vTaskDelete(NULL);
}
}
bool SchoolWiFi::download(const String url, File *file)
{
    create_progress_bar();
    downloadSuccessful = false;
    urlToDown = url;
    f_download = file;
    semaphoreDownload = xSemaphoreCreateMutex();
    xTaskCreatePinnedToCore(task_music_downloader, "MusicDownload", 8192, NULL, 1, NULL, 1);
    delay(100);
    xSemaphoreTake(semaphoreDownload, portMAX_DELAY);
    xSemaphoreGive(semaphoreDownload);
    del_progress_bar();
    delay(100);
    vSemaphoreDelete(semaphoreDownload);
    return downloadSuccessful;
}
