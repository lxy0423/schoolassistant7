#include <A_Config.h>
#define PL_MPEG_IMPLEMENTATION
#include "gui/pl_mpeg.h"
int frameid = 0;
float volume;
static int Table_fv1[256] = {-180, -179, -177, -176, -174, -173, -172, -170, -169, -167, -166, -165, -163, -162, -160, -159, -158, -156, -155, -153, -152, -151, -149, -148, -146, -145, -144, -142, -141, -139, -138, -137, -135, -134, -132, -131, -130, -128, -127, -125, -124, -123, -121, -120, -118, -117, -115, -114, -113, -111, -110, -108, -107, -106, -104, -103, -101, -100, -99, -97, -96, -94, -93, -92, -90, -89, -87, -86, -85, -83, -82, -80, -79, -78, -76, -75, -73, -72, -71, -69, -68, -66, -65, -64, -62, -61, -59, -58, -57, -55, -54, -52, -51, -50, -48, -47, -45, -44, -43, -41, -40, -38, -37, -36, -34, -33, -31, -30, -29, -27, -26, -24, -23, -22, -20, -19, -17, -16, -15, -13, -12, -10, -9, -8, -6, -5, -3, -2, 0, 1, 2, 4, 5, 7, 8, 9, 11, 12, 14, 15, 16, 18, 19, 21, 22, 23, 25, 26, 28, 29, 30, 32, 33, 35, 36, 37, 39, 40, 42, 43, 44, 46, 47, 49, 50, 51, 53, 54, 56, 57, 58, 60, 61, 63, 64, 65, 67, 68, 70, 71, 72, 74, 75, 77, 78, 79, 81, 82, 84, 85, 86, 88, 89, 91, 92, 93, 95, 96, 98, 99, 100, 102, 103, 105, 106, 107, 109, 110, 112, 113, 114, 116, 117, 119, 120, 122, 123, 124, 126, 127, 129, 130, 131, 133, 134, 136, 137, 138, 140, 141, 143, 144, 145, 147, 148, 150, 151, 152, 154, 155, 157, 158, 159, 161, 162, 164, 165, 166, 168, 169, 171, 172, 173, 175, 176, 178};
static int Table_fv2[256] = {-92, -91, -91, -90, -89, -88, -88, -87, -86, -86, -85, -84, -83, -83, -82, -81, -81, -80, -79, -78, -78, -77, -76, -76, -75, -74, -73, -73, -72, -71, -71, -70, -69, -68, -68, -67, -66, -66, -65, -64, -63, -63, -62, -61, -61, -60, -59, -58, -58, -57, -56, -56, -55, -54, -53, -53, -52, -51, -51, -50, -49, -48, -48, -47, -46, -46, -45, -44, -43, -43, -42, -41, -41, -40, -39, -38, -38, -37, -36, -36, -35, -34, -33, -33, -32, -31, -31, -30, -29, -28, -28, -27, -26, -26, -25, -24, -23, -23, -22, -21, -21, -20, -19, -18, -18, -17, -16, -16, -15, -14, -13, -13, -12, -11, -11, -10, -9, -8, -8, -7, -6, -6, -5, -4, -3, -3, -2, -1, 0, 0, 1, 2, 2, 3, 4, 5, 5, 6, 7, 7, 8, 9, 10, 10, 11, 12, 12, 13, 14, 15, 15, 16, 17, 17, 18, 19, 20, 20, 21, 22, 22, 23, 24, 25, 25, 26, 27, 27, 28, 29, 30, 30, 31, 32, 32, 33, 34, 35, 35, 36, 37, 37, 38, 39, 40, 40, 41, 42, 42, 43, 44, 45, 45, 46, 47, 47, 48, 49, 50, 50, 51, 52, 52, 53, 54, 55, 55, 56, 57, 57, 58, 59, 60, 60, 61, 62, 62, 63, 64, 65, 65, 66, 67, 67, 68, 69, 70, 70, 71, 72, 72, 73, 74, 75, 75, 76, 77, 77, 78, 79, 80, 80, 81, 82, 82, 83, 84, 85, 85, 86, 87, 87, 88, 89, 90, 90};
static int Table_fu1[256] = {-44, -44, -44, -43, -43, -43, -42, -42, -42, -41, -41, -41, -40, -40, -40, -39, -39, -39, -38, -38, -38, -37, -37, -37, -36, -36, -36, -35, -35, -35, -34, -34, -33, -33, -33, -32, -32, -32, -31, -31, -31, -30, -30, -30, -29, -29, -29, -28, -28, -28, -27, -27, -27, -26, -26, -26, -25, -25, -25, -24, -24, -24, -23, -23, -22, -22, -22, -21, -21, -21, -20, -20, -20, -19, -19, -19, -18, -18, -18, -17, -17, -17, -16, -16, -16, -15, -15, -15, -14, -14, -14, -13, -13, -13, -12, -12, -11, -11, -11, -10, -10, -10, -9, -9, -9, -8, -8, -8, -7, -7, -7, -6, -6, -6, -5, -5, -5, -4, -4, -4, -3, -3, -3, -2, -2, -2, -1, -1, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14, 14, 15, 15, 15, 16, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20, 20, 20, 21, 21, 22, 22, 22, 23, 23, 23, 24, 24, 24, 25, 25, 25, 26, 26, 26, 27, 27, 27, 28, 28, 28, 29, 29, 29, 30, 30, 30, 31, 31, 31, 32, 32, 33, 33, 33, 34, 34, 34, 35, 35, 35, 36, 36, 36, 37, 37, 37, 38, 38, 38, 39, 39, 39, 40, 40, 40, 41, 41, 41, 42, 42, 42, 43, 43};
static int Table_fu2[256] = {-227, -226, -224, -222, -220, -219, -217, -215, -213, -212, -210, -208, -206, -204, -203, -201, -199, -197, -196, -194, -192, -190, -188, -187, -185, -183, -181, -180, -178, -176, -174, -173, -171, -169, -167, -165, -164, -162, -160, -158, -157, -155, -153, -151, -149, -148, -146, -144, -142, -141, -139, -137, -135, -134, -132, -130, -128, -126, -125, -123, -121, -119, -118, -116, -114, -112, -110, -109, -107, -105, -103, -102, -100, -98, -96, -94, -93, -91, -89, -87, -86, -84, -82, -80, -79, -77, -75, -73, -71, -70, -68, -66, -64, -63, -61, -59, -57, -55, -54, -52, -50, -48, -47, -45, -43, -41, -40, -38, -36, -34, -32, -31, -29, -27, -25, -24, -22, -20, -18, -16, -15, -13, -11, -9, -8, -6, -4, -2, 0, 1, 3, 5, 7, 8, 10, 12, 14, 15, 17, 19, 21, 23, 24, 26, 28, 30, 31, 33, 35, 37, 39, 40, 42, 44, 46, 47, 49, 51, 53, 54, 56, 58, 60, 62, 63, 65, 67, 69, 70, 72, 74, 76, 78, 79, 81, 83, 85, 86, 88, 90, 92, 93, 95, 97, 99, 101, 102, 104, 106, 108, 109, 111, 113, 115, 117, 118, 120, 122, 124, 125, 127, 129, 131, 133, 134, 136, 138, 140, 141, 143, 145, 147, 148, 150, 152, 154, 156, 157, 159, 161, 163, 164, 166, 168, 170, 172, 173, 175, 177, 179, 180, 182, 184, 186, 187, 189, 191, 193, 195, 196, 198, 200, 202, 203, 205, 207, 209, 211, 212, 214, 216, 218, 219, 221, 223, 225};
static __always_inline uint8_t clamp(int n)
{
    if (n > 255)
    {
        n = 255;
    }
    else if (n & 0x80000000)
    {
        n = 0;
    }
    return n;
}

void ConvertYUV420SPToBGR(unsigned char *yData, unsigned char *vData, unsigned char *uData, unsigned char *Dst, int ImageWidth, int ImageHeight)
{
    if (ImageWidth < 1 || ImageHeight < 1 || Dst == NULL)
        return;

    int uIdx, vIdx;
    int rdif, invgdif, bdif;
    for (int i = 0; i < ImageHeight; i++)
    {
        for (int j = 0; j < ImageWidth; j++)
        {
            vIdx = (i / 2) * (ImageWidth / 2) + (j / 2);
            uIdx = vIdx;

            rdif = Table_fv1[vData[vIdx]];
            invgdif = Table_fu1[uData[uIdx]] + Table_fv2[vData[vIdx]];
            bdif = Table_fu2[uData[uIdx]];

            *(Dst++) = clamp(*yData + 20 + bdif);    // b
            *(Dst++) = clamp(*yData + 20 - invgdif); // g
            *(Dst++) = clamp(*yData + 20 + rdif);    // r
            ++yData;
        }
    }
}

uint8_t *rgb_buffer[2];
int curr_buf = 1;
// This function gets called for each decoded video frame
void my_video_callback(plm_t *plm, plm_frame_t *frame, void *user)
{
    // Do something with frame->y.data, frame->cr.data, frame->cb.data
    // plm_frame_to_bgr(frame, rgb_buffer, frame->width * 3);
    ConvertYUV420SPToBGR(frame->y.data, frame->cr.data, frame->cb.data, rgb_buffer[curr_buf], frame->width, frame->height);

    hal.display.startWrite();
    hal.display.setAddrWindow((screenWidth - frame->width) / 2, (screenHeight - frame->height) / 2, frame->width, frame->height);
    hal.display.writePixelsDMA(((void *)rgb_buffer[curr_buf]), frame->width * frame->height, true);
    // hal.display.writePixels((uint16_t *)rgb_buffer, frame->width * frame->height, false);
    hal.display.endWrite();
    curr_buf = 1 - curr_buf;
}

// This function gets called for each decoded audio frame
void my_audio_callback(plm_t *plm, plm_samples_t *frame, void *user)
{
    // Do something with samples->interleaved
    size_t dummy;
    int16_t *data = (int16_t *)malloc(2304 * 2);
    for (int i = 0; i < frame->count * 2; i++)
    {
        data[i] = frame->interleaved[i] * 32768 * volume;
    }
        i2s_write(AUDIO_I2S_DEVICE, data, frame->count * 4, &dummy, portMAX_DELAY);
    free(data);
}

static void buffer_cb(plm_buffer_t *self, void *user)
{
    uint8_t data[1436];
    size_t size = schoolWiFi.client.read(data, 1436);
    plm_buffer_write(self, data, size);
}

void VideoPlayer::play()
{
    hal.DoNotSleep = true;
    LOCKLV();
    hal.display.fillScreen(0);
    rgb_buffer[0] = (uint8_t *)ps_malloc(320 * 240 * 3);
    rgb_buffer[1] = (uint8_t *)ps_malloc(320 * 240 * 3);
    auto buffer = plm_buffer_create_with_capacity(256 * 1024);
    plm_t *plm = plm_create_with_buffer(buffer, true);
    plm_buffer_set_load_callback(buffer, buffer_cb, NULL);
    plm_set_video_decode_callback(plm, my_video_callback, NULL);
    Serial.println("初始化音频");
    // i2s configuration
    i2s_config_t i2s_config;
    memset(&i2s_config, 0, sizeof(i2s_config));
    i2s_config.sample_rate = 48000;
    i2s_config.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT;
    i2s_config.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT;
    i2s_config.intr_alloc_flags = ESP_INTR_FLAG_LEVEL1; // interrupt priority
    i2s_config.dma_buf_count = 8;
    i2s_config.dma_buf_len = 512;
    i2s_config.use_apll = 0;              // must be disabled in V2.0.1-RC1
    i2s_config.tx_desc_auto_clear = true; // new in V1.0.1
    i2s_config.fixed_mclk = I2S_PIN_NO_CHANGE;

    i2s_config.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX);
    i2s_config.communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_STAND_I2S); // Arduino vers. > 2.0.0
    i2s_driver_install((i2s_port_t)AUDIO_I2S_DEVICE, &i2s_config, 0, NULL);
    i2s_zero_dma_buffer((i2s_port_t)AUDIO_I2S_DEVICE);

    i2s_pin_config_t m_pin_config;
    m_pin_config.bck_io_num = I2S_BCLK;
    m_pin_config.ws_io_num = I2S_LRCLK; //  wclk
    m_pin_config.data_out_num = I2S_DATA;
    m_pin_config.data_in_num = -1;
    m_pin_config.mck_io_num = -1;

    const esp_err_t result = i2s_set_pin((i2s_port_t)AUDIO_I2S_DEVICE, &m_pin_config);
    assert(result == ESP_OK);

    volume = settings.getInt("settings.volume", CONFIG_DEFAULT_VOLUME);
    volume /= 255.0;
    plm_set_audio_decode_callback(plm, my_audio_callback, NULL);
    Serial.println("开始解码");

    i2s_set_sample_rates(AUDIO_I2S_DEVICE, 48000);
    // Decode
    uint32_t last_millis = millis();
    do
    {
        plm_decode(plm, millis() - last_millis);
        last_millis = millis();
        delay(3);
        if (hal.axpLongPress)
        {
            hal.axpLongPress = false;
            break;
        }
        if (hal.axpShortPress)
        {
            hal.axpShortPress = false;
            break;
        }
    } while (!plm_has_ended(plm));

    // All done
    plm_destroy(plm);
    free(rgb_buffer[0]);
    free(rgb_buffer[1]);
    i2s_driver_uninstall(AUDIO_I2S_DEVICE);
    lv_obj_invalidate(lv_scr_act());
    UNLOCKLV();
    WiFi.disconnect(true);
    hal.DoNotSleep = false;
}

VideoPlayer videoPlayer;