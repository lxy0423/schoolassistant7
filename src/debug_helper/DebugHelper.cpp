#include "DebugHelper.h"
static debugFunction_t reqf;

static void button_event_cb(lv_event_t *e)
{
    reqf = (debugFunction_t)lv_event_get_user_data(e);
}

void DebugHelper::showMenu(lv_obj_t *parent)
{
    obj = lv_obj_create(parent);
    lv_obj_set_size(obj, lv_pct(50), lv_pct(100));
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_COLUMN);
    for (uint16_t i = 0; i < count; ++i)
    {
        lv_obj_t *b;
        b = lv_btn_create(obj);
        lv_obj_t *l = lv_label_create(b);
        lv_label_set_text(l, _list[i].name);
        lv_obj_set_style_text_font(l, &lv_font_chinese_16, 0);
        lv_obj_center(l);
        lv_obj_add_event_cb(b, button_event_cb, LV_EVENT_CLICKED, (void *)_list[i].f);
    }
}

void DebugHelper::update()
{
    if(reqf)
    {
        reqf();
        reqf = NULL;
    }
}
