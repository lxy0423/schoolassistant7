# Lua 移植笔记
这里是我对网上一些资料的理解
## 常用C函数
栈完全按照后进先出规则存取，对于函数调用，参数列表中靠左的参数靠近栈底，return时靠左的值靠近栈底  
`lua_getglobal` 取出全局变量，放在栈顶  
例：
```c
lua_getglobal(L, "a");
```

`lua_pushnumber` 把数据放在栈顶，调用函数用  
`lua_pushvalue` 将栈中指定索引的元素复制一份到栈顶  
`lua_push{type}`格式的函数就算把type类型的数据放在栈顶  

`lua_pcall` 运行程序，有3个参数，第一个是Lua state，第二个是参数数量，第三个是返回值数，第4个是错误处理程序位于堆栈的位置（不懂）。  
参数依次出栈，然后是函数名，然后返回值依次入栈。  

`lua_tostring` 取出数据

---
假设函数Sum：
```lua
function Sum(...)
  local s=0
  local num=0     
  for k,v in pairs{...} do
    s = s + v
       num = k
  end
  return s,s/num
end
```
例：
```c
lua_getglobal(L, "Sum");
lua_pushnumber(L, 2);//第一个参数
lua_pushnumber(L, 3);//第二个参数
lua_pushnumber(L, 4);//第三个参数
lua_pcall(L, 3, 2, 0);  //3个参数，2个返回值，无错误处理
//此时栈中：
//（顶）,9,2,（底）
```

## 自己写库函数

### 如果要报错：`luaL_error(L, "Error message.");`
