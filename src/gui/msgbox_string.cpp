#include <A_Config.h>
static bool kb_cancel = false;
static bool kb_apply = false;
static bool str_multiline = false;
static void event_keyboard(lv_event_t *e)
{
    auto code = lv_event_get_code(e);
    switch (code)
    {
    case LV_EVENT_CANCEL:
        kb_cancel = true;
        break;
    case LV_EVENT_READY:
        kb_apply = true;
        break;
    default:
        break;
    }
}

String msgbox_string(const char *msg, bool multiline, bool passwd, bool chinese)
{
    lv_obj_t *py = NULL;
    String res = "";
    LOCKLV();
    lv_obj_t *scr_msgbox = lv_obj_create(lv_layer_top());
    lv_obj_set_size(scr_msgbox, 260, 112);
    lv_obj_align(scr_msgbox, LV_ALIGN_TOP_MID, 0, 4);
    lv_obj_pop_up(scr_msgbox, 20, 300, 0);
    lv_obj_set_style_pad_all(scr_msgbox, 0, 0);

    lv_obj_t *lblInfo = lv_label_create(scr_msgbox);
    lv_label_set_text(lblInfo, msg);
    lv_label_set_long_mode(lblInfo, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_set_style_text_font(lblInfo, &lv_font_chinese_16, 0);
    lv_obj_set_height(lblInfo, 22);
    lv_obj_set_width(lblInfo, 240);
    lv_obj_align(lblInfo, LV_ALIGN_TOP_MID, 0, 0);
    lv_obj_set_style_text_align(lblInfo, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_set_style_pad_all(lblInfo, 0, 0);

    lv_obj_t *text = lv_textarea_create(scr_msgbox);
    lv_obj_set_size(text, 240, 86);
    lv_obj_set_pos(text, 10, 20);
    lv_obj_set_style_text_font(text, &lv_font_chinese_16, 0);
    lv_textarea_set_password_mode(text, passwd);
    lv_obj_add_state(text, LV_STATE_FOCUSED);
    if (chinese)
    {
        py = lv_ime_pinyin_create(lv_layer_top());
        lv_obj_set_style_text_font(py, &lv_font_chinese_16, 0);
    }
    lv_obj_t *kb = lv_keyboard_create(lv_layer_top());
    // lv_obj_set_size(kb, LV_HOR_RES, LV_VER_RES / 2);
    lv_obj_add_event_cb(kb, event_keyboard, LV_EVENT_ALL, NULL);
    if (chinese)
    {
        lv_ime_pinyin_set_keyboard(py, kb);
        /*Get the cand_panel, and adjust its size and position*/
        lv_obj_t *cand_panel = lv_ime_pinyin_get_cand_panel(py);
        lv_obj_set_size(cand_panel, LV_PCT(100), LV_PCT(10));
        lv_obj_align_to(cand_panel, kb, LV_ALIGN_OUT_TOP_MID, 0, 0);
    }
    lv_keyboard_set_textarea(kb, text);
    lv_obj_pop_up(kb, 30, 200, 100);

    UNLOCKLV();
    kb_cancel = false;
    kb_apply = false;
    str_multiline = multiline;
    while (kb_cancel == false && kb_apply == false)
    {
        vTaskDelay(50);
    }
    LOCKLV();
    res = String(lv_textarea_get_text(text));
    lv_obj_fall_down(scr_msgbox);
    lv_obj_fall_down(kb, 30, 300, 200);
    UNLOCKLV();
    vTaskDelay(530);
    LOCKLV();
    lv_obj_del(scr_msgbox);
    if (chinese)
    {
        lv_obj_del(py);
    }
    else
    {
        lv_obj_del(kb);
    }
    UNLOCKLV();
    if (kb_cancel)
        res = "";
    return res;
}

String msgbox_passwd()
{
    return msgbox_string("请输入密码", false, true);
}