#include "A_Config.h"
static bool anim_ready_req = false;
static bool msgbox_quit_req = false;
static void msgbox_event_cb(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    if (code == LV_EVENT_CLICKED)
    {
        msgbox_quit_req = true;
    }
}

static void anim_ready_cb(lv_anim_t *a)
{
    anim_ready_req = false;
}

static void opa_set(lv_obj_t *a, int32_t opa)
{
    lv_obj_set_style_opa(a, opa, 0);
}

////////////////////////////////////////////////////////////////////////////////////////

lv_obj_t *full_screen_msgbox_create(const char *icon, const char *title,
                                    const char *str, lv_color_t bg_color)
{
    lv_obj_t *msgbox_container = lv_obj_create(lv_layer_top());
    lv_obj_t *lblicon, *lbltitle, *lblstr;
    bool usegif = false;
    LOCKLV();
    lv_obj_set_size(msgbox_container, screenWidth, screenHeight);
    lv_obj_set_x(msgbox_container, 0);
    lv_obj_set_y(msgbox_container, 0);
    lv_obj_set_style_border_width(msgbox_container, 0, 0);
    lv_obj_set_style_radius(msgbox_container, 0, 0);
    lv_obj_set_style_pad_all(msgbox_container, 0, 0);
    lv_obj_set_style_bg_color(msgbox_container, bg_color, 0);
    if (icon[1] == 0xff)
        usegif = true;
    if (usegif)
    {
        lblicon = lv_gif_create(msgbox_container);
        switch (icon[0])
        {
        case BIG_SYMBOL_BELL_NUM:
            lv_gif_set_src(lblicon, &img_gif_alarm);
            break;
        case BIG_SYMBOL_SYNC_NUM:
            lv_gif_set_src(lblicon, &img_gif_loading);
            lv_obj_set_style_bg_color(msgbox_container, FULL_SCREEN_BG_SYNC, 0);
            break;
        case BIG_SYMBOL_CHECK_NUM:
            lv_gif_set_src(lblicon, &img_gif_ok);
            lv_gif_set_repeat(lblicon, 1);
            lv_obj_set_style_bg_color(msgbox_container, FULL_SCREEN_BG_CHECK, 0);
            break;
        case BIG_SYMBOL_CROSS_NUM:
            lv_gif_set_src(lblicon, &img_gif_fail);
            lv_gif_set_repeat(lblicon, 1);
            lv_obj_set_style_bg_color(msgbox_container, FULL_SCREEN_BG_CROSS, 0);
            break;
        case BIG_SYMBOL_UPLOAD_NUM:
            lv_gif_set_src(lblicon, &img_gif_upload);
            lv_obj_set_style_bg_color(msgbox_container, FULL_SCREEN_BG_UPLOAD, 0);
            break;
        case BIG_SYMBOL_DOWNLOAD_NUM:
            lv_gif_set_src(lblicon, &img_gif_download);
            lv_obj_set_style_bg_color(msgbox_container, FULL_SCREEN_BG_DOWNLOAD, 0);
            break;
        case BIG_SYMBOL_WIFI_NUM:
            lv_gif_set_src(lblicon, &img_gif_wifi);
            lv_obj_set_style_bg_color(msgbox_container, FULL_SCREEN_BG_WIFI, 0);
            break;
        default:
            assert(false);
            break;
        }
        lv_obj_align(lblicon, LV_ALIGN_CENTER, 0, -40);
    }
    else
    {
        lblicon = lv_label_create(msgbox_container);
        //lv_obj_set_style_text_font(lblicon, &icon_64px, 0);
        lv_obj_set_style_text_color(lblicon, lv_color_white(), 0);
        lv_obj_set_style_text_align(lblicon, LV_TEXT_ALIGN_CENTER, 0);
        lv_label_set_text(lblicon, icon);
        lv_obj_set_width(lblicon, 64);
        lv_obj_set_height(lblicon, 64);
        lv_obj_align(lblicon, LV_ALIGN_CENTER, 0, -30);
        lv_obj_floating_add(lblicon, 0);
    }

    lbltitle = lv_label_create(msgbox_container);
    lv_label_set_long_mode(lbltitle, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_set_style_text_font(lbltitle, &lv_font_chinese_16, 0);
    lv_obj_set_style_text_color(lbltitle, lv_color_white(), 0);
    lv_obj_set_style_text_align(lbltitle, LV_TEXT_ALIGN_CENTER, 0);
    lv_label_set_text(lbltitle, title);
    lv_obj_set_align(lbltitle, LV_ALIGN_TOP_MID);
    lv_obj_set_width(lbltitle, lv_pct(80));

    lblstr = lv_label_create(msgbox_container);
    lv_obj_set_style_text_font(lblstr, &lv_font_chinese_16, 0);
    lv_obj_set_style_text_color(lblstr, lv_color_white(), 0);
    lv_label_set_long_mode(lblstr, LV_LABEL_LONG_WRAP);
    lv_label_set_text(lblstr, str);
    lv_obj_set_width(lblstr, screenWidth - 20);
    lv_obj_set_height(lblstr, screenHeight / 2 - 60);
    lv_obj_align(lblstr, LV_ALIGN_TOP_MID, 0, screenHeight / 2 + 50);

    lv_obj_fade_in(msgbox_container, 300, 0);
    lv_obj_add_flag(msgbox_container, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(msgbox_container, msgbox_event_cb, LV_EVENT_ALL, NULL);
    UNLOCKLV();
    msgbox_quit_req = false;
    return msgbox_container;
}

void full_screen_msgbox_del(lv_obj_t *mbox)
{
    lv_anim_t a;
    LOCKLV();
    lv_anim_init(&a);
    lv_anim_set_var(&a, mbox);
    lv_anim_set_values(&a, LV_OPA_COVER, LV_OPA_TRANSP);
    lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t)opa_set);
    lv_anim_set_time(&a, 200);
    lv_anim_set_ready_cb(&a, anim_ready_cb);
    lv_anim_start(&a);
    //lv_obj_del(lv_obj_get_child(mbox, 0));
    UNLOCKLV();
    anim_ready_req = true;

    while (anim_ready_req)
        vTaskDelay(40 / portTICK_PERIOD_MS);
    LOCKLV();
    lv_obj_del(mbox);
    UNLOCKLV();
}

void full_screen_msgbox_wait_del(lv_obj_t *mbox, uint32_t auto_back)
{
    uint32_t last_millis;
    last_millis = millis();
    while (1)
    {
        if (msgbox_quit_req || (auto_back && millis() - last_millis > auto_back))
        {
            full_screen_msgbox_del(mbox);
            return;
        }
        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
}

void full_screen_msgbox(const char *icon, const char *title,
                        const char *str, lv_color_t bg_color,
                        uint32_t auto_back)
{
    full_screen_msgbox_wait_del(full_screen_msgbox_create(icon, title, str, bg_color), auto_back);
}
