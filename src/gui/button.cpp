#include "A_Config.h"
static void button_event_cb(lv_event_t *e)
{
    bool *pressed = (bool *)lv_event_get_user_data(e);
    *pressed = true;
}
lv_obj_t *button(lv_obj_t *parent,const char *text, uint16_t x, uint16_t y, lv_event_cb_t cb)
{
    lv_obj_t *b;
    b = lv_btn_create(parent);
    lv_obj_set_pos(b, x, y);
    lv_obj_t *l = lv_label_create(b);
    lv_label_set_text(l, text);
    lv_obj_set_style_text_font(l, &lv_font_chinese_16, 0);
    lv_obj_center(l);
    if(cb)
        lv_obj_add_event_cb(b, cb, LV_EVENT_CLICKED, NULL);
    
    return b;
}

lv_obj_t *button(lv_obj_t *parent,const char *text, uint16_t x, uint16_t y, bool *var)
{
    lv_obj_t *b;
    b = lv_btn_create(parent);
    lv_obj_set_pos(b, x, y);
    lv_obj_t *l = lv_label_create(b);
    lv_label_set_text(l, text);
    lv_obj_set_style_text_font(l, &lv_font_chinese_16, 0);
    lv_obj_center(l);
    if(var)
        lv_obj_add_event_cb(b, button_event_cb, LV_EVENT_CLICKED, var);
    
    return b;
}