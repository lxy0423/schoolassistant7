#include "A_Config.h"

lv_obj_t *label(lv_obj_t* parent, const char *str, uint16_t x, uint16_t y, bool animation, uint16_t anim_delay)
{
    lv_obj_t *label1;
    label1 = lv_label_create(parent);
    lv_obj_set_x(label1, x);
    lv_obj_set_y(label1, y);
    lv_label_set_text(label1, str);
    lv_obj_set_style_text_font(label1, &lv_font_chinese_16, 0);
    if (animation)
    {
        lv_obj_fade_in(label1, 300, anim_delay);
    }
    return label1;
}
