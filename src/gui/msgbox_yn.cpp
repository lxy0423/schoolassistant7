#include "A_Config.h"
static uint8_t state_button = 0;
static void btn_yes_event_cb(lv_event_t *e)
{
    state_button = 2;
}
static void btn_no_event_cb(lv_event_t *e)
{
    state_button = 1;
}

bool msgbox_yn(const char *str)
{
    LOCKLV();
    lv_obj_t *window = lv_obj_create(lv_layer_top());
    lv_obj_t *lbl = lv_label_create(window);
    lv_obj_t *btns[2];

    lv_obj_set_align(window, LV_ALIGN_CENTER);
    lv_obj_set_size(window, 220, 140);
    lv_obj_pop_up(window);

    lv_label_set_long_mode(lbl, LV_LABEL_LONG_WRAP);
    lv_obj_set_width(lbl, lv_pct(100));
    lv_obj_set_style_text_align(lbl, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(lbl, LV_ALIGN_TOP_MID, 0, 0);
    lv_obj_set_style_text_font(lbl, &lv_font_chinese_16, 0);
    lv_label_set_text(lbl, str);

    btns[0] = lv_btn_create(window);
    btns[1] = lv_btn_create(window);
    lv_obj_set_width(btns[0], 80);
    lv_obj_set_height(btns[0], 30);
    lv_obj_set_width(btns[1], 80);
    lv_obj_set_height(btns[1], 30);

    lv_obj_align(btns[0], LV_ALIGN_BOTTOM_LEFT, 0, 0);
    lv_obj_align(btns[1], LV_ALIGN_BOTTOM_RIGHT, 0, 0);

    lv_obj_t *lbls[2];
    lbls[0] = lv_label_create(btns[0]);
    lbls[1] = lv_label_create(btns[1]);
    lv_obj_set_style_text_font(lbls[0], &lv_font_chinese_16, 0);
    lv_obj_set_style_text_font(lbls[1], &lv_font_chinese_16, 0);
    lv_label_set_text(lbls[0], "取消");
    lv_label_set_text(lbls[1], "确定");
    lv_obj_center(lbls[0]);
    lv_obj_center(lbls[1]);
    lv_obj_add_event_cb(btns[0], btn_no_event_cb, LV_EVENT_CLICKED, NULL);  /*Assign a callback to the button*/
    lv_obj_add_event_cb(btns[1], btn_yes_event_cb, LV_EVENT_CLICKED, NULL); /*Assign a callback to the button*/
    UNLOCKLV();
    state_button = 0;
    while (1)
    {
        if (state_button != 0)
        {
            state_button--;
            LOCKLV();
            lv_obj_fall_down(window);
            UNLOCKLV();
            vTaskDelay(370);
            LOCKLV();
            lv_obj_del(window);
            UNLOCKLV();
            return state_button;
        }
        vTaskDelay(50);
    }
}
