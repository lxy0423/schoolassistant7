#include "A_Config.h"

static bool done = false;
static lv_obj_t *spinbox = NULL;
static uint16_t spinbox_val;
static int32_t pos = 0;
static uint16_t getRealPos(int32_t pos_spin)
{
    uint16_t i = 1;
    switch (pos_spin)
    {
    case 1:
        i = 1;
        break;
    case 10:
        i = 10;
        break;
    case 100:
        i = 60;
        break;
    case 1000:
        i = 600;
        break;
    default:
        i = 1;
        break;
    }
    return i;
}

static void btn_plus_event(lv_event_t *e)
{
    pos = lv_spinbox_get_step(spinbox);
    spinbox_val += getRealPos(pos);
    if (spinbox_val > 23 * 60 + 59)
        spinbox_val = 0;
    lv_spinbox_set_value(spinbox, (uint16_t)(spinbox_val / 60) * 100 + spinbox_val % 60);
}

static void btn_minus_event(lv_event_t *e)
{
    pos = lv_spinbox_get_step(spinbox);
    spinbox_val -= getRealPos(pos);
    if (spinbox_val > 23 * 60 + 59)
        spinbox_val = 0;
    lv_spinbox_set_value(spinbox, (uint16_t)(spinbox_val / 60) * 100 + spinbox_val % 60);
}

static void btn_done_event(lv_event_t *e)
{
    done = true;
}

uint16_t msgbox_time(const char *str, uint16_t value_pre)
{
    done = false;
    spinbox_val = value_pre;
    LOCKLV();
    lv_obj_t *scr = lv_obj_create(lv_layer_top());
    lv_obj_set_style_pad_all(scr, 4, 0);
    lv_obj_set_size(scr, 160, 120);
    lv_obj_set_pos(scr, 0, 0);
    lv_obj_center(scr);
    spinbox = lv_spinbox_create(scr);
    lv_spinbox_set_range(spinbox, 0, 2359);
    lv_spinbox_set_digit_format(spinbox, 4, 2);
    lv_spinbox_set_value(spinbox, (uint16_t)(spinbox_val / 60) * 100 + spinbox_val % 60);
    lv_spinbox_set_rollover(spinbox, true);
    lv_spinbox_set_step(spinbox, 1000);
    lv_obj_set_width(spinbox, lv_pct(50));
    lv_obj_set_style_text_align(spinbox, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(spinbox, LV_ALIGN_CENTER, 0, -10);

    lv_obj_t *lbl = lv_label_create(scr);
    lv_obj_set_style_text_font(lbl, &lv_font_chinese_16, 0);
    lv_obj_set_style_text_align(lbl, LV_TEXT_ALIGN_CENTER, 0);
    lv_label_set_text(lbl, str);
    lv_obj_set_width(lbl, 100);
    lv_label_set_long_mode(lbl, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_align(lbl, LV_ALIGN_TOP_MID, 0, 0);

    lv_obj_align(button(scr, LV_SYMBOL_DOWN, 0, 0, btn_minus_event), LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_align(button(scr, LV_SYMBOL_UP, 0, 0, btn_plus_event), LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_t *btnOK = button(scr, LV_SYMBOL_OK, 0, 0, btn_done_event);
    lv_obj_set_width(btnOK, lv_pct(90));
    lv_obj_align(btnOK, LV_ALIGN_BOTTOM_MID, 0, 0);

    lv_obj_pop_up(scr);
    UNLOCKLV();

    while (1)
    {
        if (hal.axpShortPress)
        {
            hal.axpShortPress = false;
            lv_spinbox_step_next(spinbox);
        }
        vTaskDelay(50);
        if (done)
            break;
    }
    LOCKLV();
    lv_obj_fall_down(scr);
    UNLOCKLV();
    vTaskDelay(370);
    LOCKLV();
    lv_obj_del(scr);
    UNLOCKLV();
    return spinbox_val;
}
