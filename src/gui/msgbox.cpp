#include "A_Config.h"
static uint8_t state_button = 0;
static void btn_event_cb(lv_event_t *e)
{
    state_button = 1;
}
//TODO:autoback和标题
void msgbox(const char *prompt, const char *msg, uint32_t auto_back )
{
    LOCKLV();
    lv_obj_t *window = lv_obj_create(lv_layer_top());
    lv_obj_t *lbl = lv_label_create(window);
    lv_obj_t *btn;
    uint32_t last_millis;

    lv_obj_set_size(window, 220, 140);
    lv_obj_center(window);

    lv_label_set_long_mode(lbl, LV_LABEL_LONG_WRAP);
    lv_obj_set_width(lbl, lv_pct(100));
    lv_obj_set_style_text_align(lbl, LV_TEXT_ALIGN_LEFT, 0);
    lv_obj_align(lbl, LV_ALIGN_TOP_LEFT, 0, 20);
    lv_obj_set_style_text_font(lbl, &lv_font_chinese_16, 0);
    lv_obj_set_style_text_color(lbl, lv_palette_darken(LV_PALETTE_GREY, 1), 0);
    lv_label_set_text(lbl, msg);
    
    lbl = lv_label_create(window);      //标题
    lv_label_set_long_mode(lbl, LV_LABEL_LONG_SCROLL);
    lv_obj_set_width(lbl, lv_pct(100));
    lv_obj_set_style_text_align(lbl, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(lbl, LV_ALIGN_TOP_MID, 0, 0);
    lv_obj_set_style_text_font(lbl, &lv_font_chinese_16, 0);
    lv_label_set_text(lbl, prompt);

    btn = lv_btn_create(window);
    lv_obj_set_width(btn, 80);
    lv_obj_set_height(btn, 30);

    lv_obj_align(btn, LV_ALIGN_BOTTOM_RIGHT, 0, 0);

    lv_obj_t *l;
    l= lv_label_create(btn);
    lv_obj_set_style_text_font(l, &lv_font_chinese_16, 0);
    lv_label_set_text(l, "确定");
    lv_obj_center(l);
    lv_obj_add_event_cb(btn, btn_event_cb, LV_EVENT_CLICKED, NULL);  /*Assign a callback to the button*/
    lv_obj_pop_up(window);
    UNLOCKLV();
    state_button = 0;
    last_millis = millis();
    while (1)
    {
        if (state_button != 0 || (auto_back != 0 && millis() - last_millis > auto_back))
        {
            LOCKLV();
            lv_obj_fall_down(window);
            UNLOCKLV();
            vTaskDelay(370);
            LOCKLV();
            lv_obj_del(window);
            UNLOCKLV();
            return;
        }
        vTaskDelay(50);
    }
}
