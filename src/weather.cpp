#include "A_Config.h"
#include <HTTPClient.h>
Weather weather;
/**
 * @brief 移动stream，直到找到某个字符串
 * @return 是否找到这个字符串
 */
/*
bool gotoStr(WiFiClient &stream, String str)
{
    char *strchr = (char *)malloc(str.length() + 1);
    char *strqueue = (char *)malloc(str.length() + 1); //查找队列
    int16_t strlength = str.length();
    int8_t queuehead = 0;    //队列头指针
    int8_t queuereadptr = 0; //队列读取指针
    uint8_t i;               //目标字符串位置指针
    bool found = false;
    bool notfound = false;
    char c;
    strcpy(strchr, str.c_str());
    while (stream.available())
    {
        stream.readBytes(&c, 1);
        strchr[queuehead] = c;
        ++queuehead;
        if (queuehead == strlength)
            queuehead = 0;
        queuereadptr = queuehead;
        notfound = false;
        i = 0;
        while (i < strlength)
        {
            if (strqueue[queuereadptr] != strchr[i])
            {
                notfound = true;
                break;
            }
            ++queuereadptr;
            ++i;
            if (queuereadptr == strlength)
                queuereadptr = 0;
        }
        if (notfound == false)
        {
            found = true;
            break;
        }
    }
    free(strchr);
    free(strqueue);
    return found;
}*/

void Weather::begin()
{
    started = false;
    File file = SPIFFS.open("/weather.bin", "r");
    if (!file)
        return;
    if (file.readBytes((char *)&hour24, sizeof(hour24)) == sizeof(hour24))
    {
        file.readBytes((char *)&desc1, sizeof(desc1));
        file.readBytes((char *)&desc2, sizeof(desc2));
        if (file.readBytes((char *)&rain, sizeof(rain)) == sizeof(rain))
        {
            started = true;
        }
    }
    file.close();
}

int8_t Weather::refresh()
{
    schoolWiFi.http.addHeader("Accept", "*/*");
    schoolWiFi.http.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");
    schoolWiFi.http.begin("http://api.caiyunapp.com/v2.5/96Ly7wgKGq6FhllM/" WEATHER_LOCATION "/weather.jsonp?hourlysteps=120"); // HTTP
    // http.begin("http://api.caiyunapp.com/v2.5/96Ly7wgKGq6FhllM/116.0684%2C39.4978/weather.jsonp?unit=metric%3Av2"); //HTTP
    schoolWiFi.http.addHeader("Accept", "*/*");
    schoolWiFi.http.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36");

    int httpCode = schoolWiFi.http.GET();

    if (httpCode == HTTP_CODE_OK)
    {
        DynamicJsonDocument doc(50000);
        auto s = schoolWiFi.http.getStream();
        deserializeJson(doc, s);
        if (doc["status"] != "ok")
        {
            lv_toast("API已失效");
            schoolWiFi.http.end();
            doc.clear();
            return -3;
        }
        strcpy(desc2, doc["result"]["minutely"]["description"].as<String>().c_str());
        strcpy(desc1, doc["result"]["hourly"]["description"].as<String>().c_str());
        for (uint8_t i = 0; i < 120; ++i)
        {
            //下面获取当前时间
            String timestr;
            timestr = doc["result"]["hourly"]["temperature"][i]["datetime"].as<String>();
            timestr = timestr.substring(5, 13);
            strcpy(hour24[i].date, timestr.c_str());
            //天气类型、气温、风速
            String s = doc["result"]["hourly"]["skycon"][i]["value"].as<String>();
            hour24[i].weathernum = codeToNum(s.c_str());
            hour24[i].temperature = int16_t(doc["result"]["hourly"]["temperature"][i]["value"].as<float>() * 10);
            hour24[i].winddirection = uint16_t(doc["result"]["hourly"]["wind"][i]["direction"].as<float>());
            hour24[i].windspeed = uint16_t(doc["result"]["hourly"]["wind"][i]["speed"].as<float>() * 10);
            hour24[i].rain = uint16_t(doc["result"]["hourly"]["precipitation"][i]["value"].as<float>() * 100);
            //降水
            rain[i] = doc["result"]["minutely"]["precipitation_2h"][i].as<float>() * 1000;
        }
        doc.clear();
    }
    else
    {
        lv_toast("HTTP GET错误");
        schoolWiFi.http.end();
        return -2;
    }
    schoolWiFi.http.end();
    started = true;
    save();
    lv_toast("天气更新成功");
    return 0;
}

void Weather::save()
{
    File file = SPIFFS.open("/weather.bin", "w");
    if (!file)
        return;
    file.write((uint8_t *)&hour24, sizeof(hour24));
    file.write((uint8_t *)&desc1, sizeof(desc1));
    file.write((uint8_t *)&desc2, sizeof(desc2));
    file.write((uint8_t *)&rain, sizeof(rain));
    file.close();
}

weatherInfo24H *Weather::getWeather(uint8_t month, uint8_t date, uint8_t hour)
{
    if (started == false)
        return NULL;
    char strdate[9];
    sprintf(strdate, "%02d-%02dT%02d", month, date, hour);
    for (uint8_t i = 0; i < 48; ++i)
    {
        if (strcmp(strdate, hour24[i].date) == 0)
        {
            return &hour24[i];
        }
    }
    return NULL;
}
